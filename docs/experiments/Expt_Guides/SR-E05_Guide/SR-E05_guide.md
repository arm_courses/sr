# 《实验05：需求规格说明与需求管理》实验指导

<!-- @import " [TOC] " {cmd="toc" depthFrom=2 depthTo=6 orderedList=true} -->

<!-- code_chunk_output -->

1. [实验子项01实验指导](#实验子项01实验指导)
    1. [实验子项01实验内容](#实验子项01实验内容)
    2. [实验子项01实验步骤](#实验子项01实验步骤)
    3. [实验子项01实验参考结果](#实验子项01实验参考结果)
2. [实验子项02实验指导](#实验子项02实验指导)
    1. [实验子项02实验内容](#实验子项02实验内容)
    2. [实验子项02实验步骤](#实验子项02实验步骤)
    3. [实验子项02实验参考结果](#实验子项02实验参考结果)

<!-- /code_chunk_output -->

## 实验子项01实验指导

### 实验子项01实验内容

:book:P514-523

根据“食堂订餐系统（COS）”项目情况，进行需求开发，使用Markdown或类似工具编写一份较为完整的SRS，该文档应至少包括自然语言描述、半形式化的图示模型描述

### 实验子项01实验步骤

1. 在OS下新建一个名为 **`"COS_Development"`** 的文件夹和 **`00_Diagram`** 子文件夹，在VSCode中打开该文件夹，在该文件夹下新建一个名为 **`COS_SRS.md`** 的文件
1. 参照:book:P514的内容，将以下部分内容输入到 **`COS_SRS.md`** 文件中
    1. **`1. 介绍`**
    1. **`2. 总述`**
        + 不需要添加 **`图C-2`**
    1. **`3. 系统特性`**
        + 不需要添加 **`3.1.2 功能性需求`** 中的表格
    1. **`4. 数据需求`**
        + 不需要添加 **`4.2 数据字典`** 中的表格
        + 将 **`SR-E04`** 实验的ERD图复制到 **`"COS_Development/00_Diagram"`** 中，并在MD文件中链接
    1. **`5. 外部接口需求`**
    1. **`6. 质量属性`**
    1. **`附录A: 分析模型`**
        + 将 **`SR-E04`** 实验的SD图复制到 **`"COS_Development/00_Diagram"`** 中，并在MD文件中链接

### 实验子项01实验参考结果

:point_right:**说明：可由`Markdown Preview Enhanced: Create Toc`命令生成自动的目录（Table Of Content, TOC），详见 <https://shd101wyy.github.io/markdown-preview-enhanced/#/zh-cn/toc> 文档说明**

![01_SRS_ret](./00_Image/01_SRS_ret.png)

## 实验子项02实验指导

### 实验子项02实验内容

根据“食堂订餐系统（COS）”的SRS结果，对其进行需求变更，利用SCM工具进行配置项管理和版本控制

### 实验子项02实验步骤

**前置步骤：** 打开CLI（**`Git Bash`** 或 **`CMD`**）并进入 **`COS_Development`** 目录

1. **Initialize Repository：** 运行 **`git init .`** 命令初始化repository
1. **Add to Index：** 运行 **`git add .`** 命令将working directory里的文件加入到index
1. **Commit to Repository：** 运行 **`git commit -m"init with first SRS"`** 将index提交至Repository
1. **Create and Switch to Branch：** 运行 **`git checkout -b dev`** 创建并切换到dev分支
1. **Simulate Change：** 打开 **`COS_SRS.md`** 并对其进行一些修改（任意修改均可）
1. **Check Difference：** 运行 **`git diff COS_SRS.md`** 查看差异
1. **Add && Commit Change to Branch：** 运行 **`git commit -am"modify requirement"`**
1. **Switch to master：** 运行 **`git switch master`** 切换到master分支
1. **Merge dev to Master：** 运行 **`git merge dev`** 合并dev分支到master分支
1. **View Result：** 运行 **`git log --oneline --graph`** 查看分支操作结果

### 实验子项02实验参考结果

**:warning:注意：以下截图中`COS_Development$`不是命令，而是CLI的PS1提示符**

![git_ret_01](./00_Image/git_ret_01.png)

![git_ret_02](./00_Image/git_ret_02.png)

![git_ret_03](./00_Image/git_ret_03.png)

![git_ret_04](./00_Image/git_ret_04.png)

![git_ret_05](./00_Image/git_ret_05.png)
