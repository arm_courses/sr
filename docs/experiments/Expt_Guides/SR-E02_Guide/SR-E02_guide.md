# 《实验02：软件需求环境的搭建与应用》实验指导

<!-- @import " [TOC] " {cmd="toc" depthFrom=2 depthTo=6 orderedList=true} -->

<!-- code_chunk_output -->

1. [概述](#概述)
2. [安装包下载地址](#安装包下载地址)
3. [实验操作步骤](#实验操作步骤)
    1. [GraphViz安装指引](#graphviz安装指引)
    2. [Git安装指引](#git安装指引)
    3. [VSCode安装指引](#vscode安装指引)
    4. [VSCode插件安装指引](#vscode插件安装指引)
        1. [VSCode插件在线安装方式](#vscode插件在线安装方式)
        2. [VSCode插件离线安装方式](#vscode插件离线安装方式)
4. [实验验证步骤](#实验验证步骤)
    1. [SR环境搭建验证说明](#sr环境搭建验证说明)
    2. [SR环境搭建验证前置步骤](#sr环境搭建验证前置步骤)
    3. [验证GraphViz Preview插件安装](#验证graphviz-preview插件安装)
    4. [验证PlantUML插件安装](#验证plantuml插件安装)
    5. [验证Markdown Preview Enhanced插件安装](#验证markdown-preview-enhanced插件安装)
    6. [验证Git与VSCode集成](#验证git与vscode集成)
        1. [`git init`建立`repo`](#git-init建立repo)
        2. [`git commit`提交](#git-commit提交)
5. [延伸参考资料](#延伸参考资料)
    1. [官方参考资料](#官方参考资料)
    2. [第三方参考资料（文档或教程类）](#第三方参考资料文档或教程类)
    3. [第三方参考资料（多媒体类）](#第三方参考资料多媒体类)
    4. [辅助工具或其他](#辅助工具或其他)

<!-- /code_chunk_output -->

## 概述

软件需求分析环境需要满足三个功能需求：**文档撰写**、**图示建模** 和 **版本控制**，因此，以 `标记语言Markup Language`、`建模语言Modeling Language` 和 `软件配置管理Software Configuration Management` 三类工具分别满足这三个功能需求，而这三类工具分别采用以下具体工具：

1. **`标记语言Markup Language`：** `Markdown`语言，具体采用的工具是 **[`markdown-preview-enhanced`](https://shd101wyy.github.io/markdown-preview-enhanced/#/)**
1. **`建模语言Modeling Language`：** `UML`语言，具体采用的工具是 **[`PlantUML`](http://plantuml.com/)**
1. **`软件配置管理Software Configuration Management`：** ，具体采用的工具是 **[`Git`](https://git-scm.com/)**

## 安装包下载地址

1. **下载并安装`GraphViz`（ <https://graphviz.org/download/> ）并配置环境变量：** `PlantUML` 底层是通过`GraphViz`进行渲染（`GraphViz`使用的渲染语言叫`dot`），故需要安装`GraphViz`，由于需要被`VSCode`调用，故需要配置环境变量
1. **下载并安装`Git`（ <https://git-scm.com/download/> ） 并配置环境变量**，由于需要被`VSCode`调用，故需要环境变量（ **默认情况下`Git for Windows Setup`版安装过程中可以选择配置环境变量，因此安装完后不需要手动再配置环境变量**）
1. **下载并安装`JDK`或`JRE`并配置环境变量：** `PlantUML`图形渲染基于Java运行环境
1. **下载并安装`Visual Studio Code（以下简称VSCode）`（ <https://code.visualstudio.com/#alt-downloads> ）**
1. **安装`VSCode`插件：**
    1. **在`VSCode`的插件管理中搜索并安装`Markdown Preview Enhanced`（作者是：Yiyi Wang）**
    1. **在`VSCode`的插件管理中搜索并安装`GraphViz Preview`（作者是：EFanZh）**
    1. **在`VSCode`的插件管理中搜索并安装`PlantUML`（作者是：jebbs）**

:point_right: **备注：**

1. `VSCode`、`Git`、`JDK`区分32-bit和64-bit两种安装包，`GraphViz`不区分
1. `VSCode`请下载并安装`System Installer`版（`User Installer`版可能存在权限不足的问题）
1. SR环境以`VSCode`为主环境，不需要通过`CLI`直接使用`PlantUML`（`VSCode`的`PlantUML`插件已自带`PlantUML`的`jar`包），因此，不需要单独安装`PlantUML`（如需通过`CLI`直接使用`PlantUML`，请参照 <http://plantuml.com/starting> 的指引安装）
1. `VSCode`插件可在 <https://marketplace.visualstudio.com/vscode> 搜索对应插件后下载 **离线安装包**

## 实验操作步骤

**说明：**

1. 以下假定已安装`JDK`并配置环境变量
1. 安装路径请尽量避免包含非英文字符和特殊字符（包含非英文字符和特殊字符时可能会产生用户不可预知的错误）

### GraphViz安装指引

1. 下载对应`OS`的`GraphViz`安装包并按提示安装
1. 将 **`GraphViz`的`bin`目录** 添加到系统环境变量`Path`中
1. 打开`CLI`（如`cmd`）输入`dot -V`显示出`Graphviz`版本号时表示已安装并配置成功

**:warning::warning::warning:注意：:warning::warning::warning:**

1. 环境变量配置请 **以实际安装路径为准**
    1. 64位`windows`下`GraphViz`的`bin`目录默认为`C:\Program Files (x86)\Graphviz<$version>\bin`
    1. 32位`windows`下`GraphViz`的`bin`目录默认为`C:\Program Files\Graphviz<$version>\bin`
    1. `Graphviz<$version>`中的`<$version>`表示版本号
1. 程序在启动时读取环境变量，因此，**如果配置环境变量时已打开程序，请重启程序以便读取新的设置**

![graphviz_insall_note_01](./00_Images/graphviz_insall_note_01.png)

![graphviz_insall_note_02](./00_Images/graphviz_insall_note_02.png)

### Git安装指引

1. 下载对应`OS`的`Git`安装包并按提示安装
1. 安装过程中保持默认选项，请留意以下截图的选项
1. 安装完成后，打开`Git Bash`或`CMD`输入`git --version`可显示`Git`版本号时表示`Git`安装成功

**说明：**

1. `Git Bash`可通过`[Windows开始菜单]->[所有程序]->[Git]->[Git Bassh]`或`<$git_install_path>/git-bash.exe`打开
1. `Git Bash`使用`MinGW Minimalist GNU for Windows`作为`POSIX GCC`兼容开发环境，可以使用`Git Bash`运行部分`POSIX`命令（包括`Vim`编辑器）

![git_install_note_01](./00_Images/git_install_note_01.png)

![git_install_note_02](./00_Images/git_install_note_02.png)

![git_install_note_03](./00_Images/git_install_note_03.png)

![git_install_note_04](./00_Images/git_install_note_04.png)

### VSCode安装指引

下载对应`OS`的`VSCode`安装包（`System Installer`版）并按提示安装，请注意以下截图所示：

![VSCode_install_note_01](./00_Images/VSCode_install_note_01.png)

### VSCode插件安装指引

#### VSCode插件在线安装方式

启动`VSCode`后，同时按下`Ctrl + Shift + X`（`x`代表`eXtensions`）或在菜单栏中选择 `[View] -> [Extensions]`打开插件界面，搜索时，搜索结果一般按插件被安装量排序

1. 搜索`Markdown`，找到 **`Markdown Preview Enhanced`（作者是：Yiyi Wang）** 并点击`Install`
1. 搜索`GraphViz`，找到 **`GraphViz Preview`（作者是：EFanZh）** 并点击`Install`
1. 搜索`PlantUML`，找到 **`PlantUML`（作者是：jebbs）** 并点击`Install`

![VSCode_plugin_install_note_01](./00_Images/VSCode_ext_install_note_01.png)

![VSCode_plugin_install_note_02](./00_Images/VSCode_ext_install_note_02.png)

#### VSCode插件离线安装方式

启动`VSCode`后，同时按下`Ctrl + Shift + X`（`x`代表`eXtensions`）或在菜单栏中选择 `[View] -> [Extensions]`打开插件界面，按下面步骤打开离线安装插件文件选择对话框后即可离线安装插件。

![extension_offline_install](./00_Image/extension_offline_install.png)

## 实验验证步骤

### SR环境搭建验证说明

1. `PlantUML`的图形渲染基于`GraphViz`（`sequence diagrams`和`activity (beta) diagrams`不需要`GraphViz`）
1. 以下SR环境搭建验证以`VSCode`作为集成环境进行验证（即：验证是否能成功使用各工具）

### SR环境搭建验证前置步骤

1. 在OS环境下新建一个文件夹（`VSCode`中无法直接新建顶层文件夹），并在`VSCode`中打开该文件夹： `VSCode`可以通过`workspace`方式打开多个`folder`，也可单独打开单个`folder`（可选保存为`workspace`），以下指引在OS环境下新建一个`folder`并命名为`WS_VSCode`，并在`VSCode`中打开（`[Fle] ==> [Open Folder...]`）
1. 在`VSCode`中新建子文件夹： 在`VSCode`中新建一个文件夹`00_Diagram`（在`Explorer`窗口中右键单击后选择`[New Folder]`）

**说明：** `WS_VSCode`和`00_Diagram`文件夹名可按自己的习惯另行命名

![VSCode_usage_note_01](./00_Images/VSCode_usage_note_01.png)

![VSCode_usage_note_02](./00_Images/VSCode_usage_note_02.png)

![VSCode_usage_note_03](./00_Images/VSCode_usage_note_03.png)

### 验证GraphViz Preview插件安装

1. 在`00_Diagram`中新建一个名为`hello_graphviz.gv`的文件
1. 在`hello_graphviz.gv`文件中输入以下内容
1. 按下`Ctrl + Shift + P`并输入或选择`graphviz: open preview to the side`命令
1. 能正确渲染得到图形则表示插件安装成功并将`GraphViz`与`VSCode`集成

```{.line-numbers}
digraph hello_graphviz{
    node[shape=box];
    hello -> graphviz;
}
```

![graphviz_verify_01](./00_Images/graphviz_verify_01.png)

### 验证PlantUML插件安装

1. 在`00_Diagram`中新建一个名为`hello_plantuml.puml`的文件
1. 在`hello_plantuml.puml`文件中输入以下内容
1. 按下`Ctrl + Shift + P`并输入或选择`plantuml: preview current diagram`
1. 能正确渲染得到图形则表示`PlantUML`插件安装成功并能成功调用`GraphViz`

```{.line-numbers}
@startuml
actor student
usecase learn_SR as "Learning the Software Requirements"
student -> learn_SR
@enduml
```

![plantuml_verify_01](./00_Images/plantuml_verify_01.png)

### 验证Markdown Preview Enhanced插件安装

1. 在`ws_vscode`中新建一个名为`hello_markdown.md`的文件
1. 在`hello_markdown.md`文件中输入以下内容（**:warning:注意：请特别注意所嵌入图示文件的路径**）
1. 按下`Ctrl + Shift + P`并输入或选择`markdown preview enhanced: open preview to the side`命令
1. 能正确渲染得到预览和图形即表示工具集成成功

```markdown{.line-numbers}
# 这是一级标题

## 这是二级标题

以下是嵌入`Graphviz`和`PlantUML`图示

@import "./00_Diagram/hello_graphviz.gv"

@import "./00_Diagram/hello_plantuml.puml"
```

![mpe_verify_01](./00_Images/mpe_verify_01.png)

### 验证Git与VSCode集成

#### `git init`建立`repo`

1. `VSCode`打开的`folder`未建立`repository`（以下使用简称`repo`）时`scm`窗口显示`No source control providers registered`
1. 同时按下`Ctrl + Shift + P`打开`Command Palette`输入或选择`git initialize repository`命令，该命令将为`folder`建立一个`repo`

![vscode_git_usage_note_01](./00_Images/vscode_git_usage_note_01.png)

![vscode_git_usage_note_02](./00_Images/vscode_git_usage_note_02.png)

![vscode_git_usage_note_03](./00_Images/vscode_git_usage_note_03.png)

#### `git commit`提交

按以下截图输入并按步骤操作

![git_verify_04](./00_Images/git_verify_04.png)

会得到以下错误提示，原因是未配置`Git`的邮箱地址和用户名

![git_verify_05](./00_Images/git_verify_05.png)

选择`[Terminal] ==> [New Terminal]`打开一个`Terminal`，按以下方式配置`Git`的邮箱地址和用户名

![git_verify_06](./00_Images/git_verify_06.png)

![git_verify_07](./00_Images/git_verify_07.png)

重复步骤如下

![git_verify_04](./00_Images/git_verify_04_tmgk8sbkv.png)

在`Terminal`中输入`git log`得到类似以下输出，说明`git commit`成功

![git_verify_08](./00_Images/git_verify_08.png)

## 延伸参考资料

### 官方参考资料

1. VSCode官方网址： https://code.visualstudio.com/
1. VSCode官方帮助文档： https://code.visualstudio.com/docs
1. Markdown Preview Enhanced帮助文档（含`markdown`语法、图示绘制及其他相关帮助文档）： https://shd101wyy.github.io/markdown-preview-enhanced/#/
1. GraphViz官方网址： https://graphviz.gitlab.io/
1. GraphViz官方帮助文档： https://graphviz.gitlab.io/documentation/
1. PlantUML官方网址及官方帮助文档： http://plantuml.com/
1. Git官方网址：https://git-scm.com
1. Git官方帮助文档及Cheat Sheet： https://git-scm.com/doc
1. MinGW官方网址： http://www.mingw.org/

### 第三方参考资料（文档或教程类）

1. 廖雪峰-Git教程： https://www.liaoxuefeng.com/wiki/896043488029600
1. Ashley’s PlantUML Documentation： https://plantuml-documentation.readthedocs.io/en/latest/index.html
1. 菜鸟教程-Markdown： https://www.runoob.com/markdown/md-tutorial.html
1. W3Cschool-UML： https://www.w3cschool.cn/uml_tutorial/
1. 菜鸟教程-Git： https://www.runoob.com/git/git-tutorial.html
1. W3Cschool-Git： https://www.w3cschool.cn/git/

### 第三方参考资料（多媒体类）

1. 代码时间-吕鹏（`VSCode`开发者之一）的访谈播客： http://codetimecn.com/episodes/vsc

### 辅助工具或其他

1. Planttext网上编辑器及示例： https://www.planttext.com/
