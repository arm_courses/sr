# 需求获取与用例建模

<!-- @import " [TOC] " {cmd="toc" depthFrom=2 depthTo=6 orderedList=true} -->

<!-- code_chunk_output -->

1. [实验子项01实验指导](#实验子项01实验指导)
    1. [实验子项01实验内容](#实验子项01实验内容)
    2. [实验子项01实验参考资料](#实验子项01实验参考资料)
    3. [实验子项01实验参考结果](#实验子项01实验参考结果)
2. [实验子项02实验指导](#实验子项02实验指导)
    1. [实验子项02实验内容](#实验子项02实验内容)
    2. [实验子项02实验参考资料](#实验子项02实验参考资料)
    3. [实验子项02实验参考结果](#实验子项02实验参考结果)

<!-- /code_chunk_output -->

## 实验子项01实验指导

### 实验子项01实验内容

根据"化学品跟踪系统（CTS）"项目需求，使用UML用例图技术绘制用例图

### 实验子项01实验参考资料

1. :book:教材《软件需求（第3版）》**P132"图8-2 化学品跟踪系统的部分用例图"**
1. :bookmark:**[use_case_diagram.md](../../../../Extras/02_PlantUML/02_Use_Case_Diagram/use_case_diagram.md)** 的内容

### 实验子项01实验参考结果

@import "../../Expt_Anses/SR-E03_Ans/00_Diagram/CTS_UC.puml"

## 实验子项02实验指导

### 实验子项02实验内容

根据"食堂订餐系统（COS）"项目背景，获取用户需求，使用UML用例图技术绘制用例图

### 实验子项02实验参考资料

1. :book:教材《软件需求（第3版）》**P512第一个表格**
1. :bookmark:**[use_case_diagram.md](../../../../Extras/02_PlantUML/02_Use_Case_Diagram/use_case_diagram.md)** 的内容

### 实验子项02实验参考结果

@import "../../Expt_Anses/SR-E03_Ans/00_Diagram/COS_UC_01.puml"

@import "../../Expt_Anses/SR-E03_Ans/00_Diagram/COS_UC_02.puml"

@import "../../Expt_Anses/SR-E03_Ans/00_Diagram/COS_UC_03.puml"

@import "../../Expt_Anses/SR-E03_Ans/00_Diagram/COS_UC_04.puml"
