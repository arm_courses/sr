---
presentation:
    theme: white.css
    width: 1200
    height: 800
    slideNumber: true
    enableSpeakerNotes: true
    transition: zoom
    transitionSpeed: default
---

<!-- slide -->
# 第09章：照章办事Playing by the Rules:book:P147

<!-- slide -->
# :book:P147案例

1. 系统遵循业务规则的规定
1. 系统强化业务规则的实施

<!-- slide -->
# 本章重点和难点

1. 本章重点：
    1. 业务规则与业务规则的特点
    1. 业务规则与软件需求的关系
1. 本章难点：
    1. 业务规则库
    1. 业务规则的5种类型

<!-- slide -->
# 业务规则:book:P147

>业务规则是指对业务定义和约束的描述，用于维持业务结构或控制和影响业务的行为。业务规则包括：政府法规、工业行业标准、公司政策、计算方法等

1. 合规原则： 业务运营需要遵守政策、法规以及行业标准
1. 规则保障： 业务规则需要人工和系统的政策和流程保证
1. 系统之外： 大多数业务规则起源于特定的软件应用之外，业务规则是业务的属性
1. 需求来源： 它们决定着系统必须具备怎样的属性才能符合规范

<!-- slide -->
# 业务规则、业务过程和业务需求:book:P147

:warning:业务规则、业务过程、业务需求是相关但不相同的概念

1. 业务规则通过建立词汇表、限制、触发行动或监控运算等方式影响业务过程
1. 同一个业务规则可能应用到多个流程中
1. 业务规则是一组独立的信息

<!-- slide -->
# 业务规则与软件需求:book:P148表9-1

|Requirement type|Illustration of business rules’ influence|Example|
|--|--|--|
|Business requirement|Government regulations can lead to necessary business objectives for a project.|The Chemical Tracking System must enable compliance with all federal and state chemical usage and disposal reporting regulations within five months.
|User requirement|Privacy policies dictate which users can and cannot perform certain tasks with the system.|Only laboratory managers are allowed to generate chemical exposure reports for anyone other than themselves.|

<!-- slide -->
# 业务规则与软件需求:book:P148表9-1（续）

|Requirement type|Illustration of business rules’ influence|Example|
|--|--|--|
|Functional requirement|Company policy is that all vendors must be registered and approved before an invoice will be paid.|If an invoice is received from an unregistered vendor, the Supplier System shall email the vendor editable PDF versions of the supplier intake form and the W-9 form.|
|Quality attribute|Regulations from government agencies, such as OSHA and EPA, can dictate safety requirements, which must be enforced through system functionality.|The system must maintain safety training records, which it must check to ensure that users are properly trained before they can request a hazardous chemical.|

<!-- slide -->
# 业务规则库:book:P148

:point_right:企业应当建立一致的统一的业务规则库

1. 并不是所有公司都会将其核心业务规则当作珍贵的企业资产
1. 如果业务规则不加以正确记录和管理，就会局限于少数人的大脑中，BA需要知道向谁打听影响系统的规则
1. 系统应当以一致的方式遵循策略（即业务规则）

<!-- slide -->
# 业务规则分类法:book:P149

1. 从业务角度： 描述了在某个特定活动或环境中的行为、行动、实践或流程所应尽的义务
1. 从系统角度： 描述了对业务某些方面的定义或限制的声明

![httpatomoreillycomsourcemspimages1792297](./00_Images/httpatomoreillycomsourcemspimages1792297.png)

<!-- slide -->
# 业务规则的类型:book:P149

1. 事实： 对业务在某个特定时间点简单而正确的陈述
1. 约束： 限制系统或其用户可执行的行为
1. 触发规则： 当特定条件满足时触发某些活动的规则
1. 推理： 从已知事实中产生新的事实
1. 运算： 使用特定的数学公式或算法将已知数据加工为新的数据

:warning::exclamation:以原子级别记录业务规则，而不是在一条规则中组合多条子规则

<!-- slide -->
# 发现业务规则:book:P156

![httpatomoreillycomsourcemspimages1792299](./00_Images/httpatomoreillycomsourcemspimages1792299.png)

<!-- slide -->
# 记录业务规则:book:P154

1. 用结构化的模板来定义不同类型的规则
1. 为每个业务规则分配一个ID

:warning::exclamation:对于动态规则不应该硬编码进系统代码中

<!-- slide -->
# 业务规则与需求记录:book:P157

1. 识别并记录业务规则后，需要决定哪些业务规则必须在系统中实现
1. BA须决定哪些规则与自己的系统有关，哪些须由系统强制执行、如何强制执行（人工审批？规则运算？）

:point_right:使用工具将业务规则与功能需求相关联，而不是简单地复制到SRS中

<!-- slide -->
# Thank You

## :ok:End of This Chapter:ok:
