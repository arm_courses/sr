---
presentation:
    theme: white.css
    width: 1200
    height: 800
    slideNumber: true
    enableSpeakerNotes: true
    transition: zoom
    transitionSpeed: default
---

<!-- slide -->
# 第16章： 要事优先、设定需求优先级First Things First: Setting Requirement Priorities:book:P279

<!-- slide -->
# :book:P279案例

1. 凭什么要做需求优先级排序？它们都很重要，否则也不会提出这些需求
1. 项目不可能同时交付全部功能，因此，需要达成共识以便实现需要 **优先实现的功能集**

<!-- slide -->
# 本章重点和难点

1. 本章重点：
    1. 优先级排序的原因？
    1. 三层分级法的定义和使用方法
    1. 质量功能展开法作为明确而严谨的结构化的优先级排序方法
1. 本章难点：
    1. 优先级排序要考虑哪些因素/属性？
    1. 优先级排序的各种方法优缺点

<!-- slide -->
# 为什么要优先级排序Why Prioritize Requirements?:book:P280

## :point_right:项目不可能同时交付全部功能，需要解决有限资源与大量需求间的矛盾，在受项目条件约束的情况下，尽快将会最大的业务价值

>The product **backlog** is **a breakdown of work to be done** and contains **an ordered list of product requirements** that a scrum team maintains for a product. Common formats include **user stories** and **use cases**.

<!-- slide -->
# 人与优先级间的博弈Games People Play with Priorities:book:P282

1. 客户认为每个需求都有高优先级
1. 评估优先级时，要了解需求间的连接和内在关系，以及是否与项目业务目标相匹配

<!-- slide -->
# 优先级排序实践Some Prioritization Pragmatics:book:P281

1. 优先级排序是一个动态的、持续进行的过程
1. 为优先级排序选择合适的抽象级别：特性、用例、用户故事或功能需求
1. 优先级排序需要各类干系人参与

<!-- slide -->
# 优先级排序实践需考虑的因素

1. 客户要求： 客户认为的优先级程度
1. 重要程度： 需求对客户的相对重要程度
1. 交付时间： 功能需要交付的时间
1. 前后关系： 需求间的前继后续关系以及其他关系
1. 不可分割： 必须放在一起实现的需求
1. 成本因素： 每个需求所需要的成本

<!-- slide -->
# 确定优先级的技术Some Prioritization Techniques:book:P283

1. **结构化方法以消除干扰因素：** 在干系人众多的大型或有争议的项目中，需要一种结构化更强的方法，力求在优先级排序过程中消除一些干扰因素
1. **量化因素以辅助优先级排序：** 一些统计学和数学技术被用于辅助优先级排序，优先级最高的需求提供最大比例的产品总价值以及最小比例的总成本

<!-- slide -->
# 优先级排序技术

1. **入选与落选二分判定：** 二分判定直至仅剩首个版本最基础的必要需求
1. **两两比较计算分值：** 适合需求数量不大情况下使用，如少于20个需求数量
1. **:point_right:三层分级法（重要-紧急法）：** 从 **重要性** 和 **紧急性** 两个维度评估优先级并将优先级划分成三个优先级层级的一种优先级排序技术，可以进行 **多轮的三层分级再合并** 以符合项目实际情况
    1. 高优先级： 重要且紧急
    1. 中优先级： 重要不紧急
    1. 低优先级： 不重要且不紧急、紧急不重要
1. MoSCoW： Must/Should/Could/Won't
1. **100美元法：** 有限资源的分配

<!-- slide -->
# 基于重要性和紧急程度的优先级排序:book:P285图16-1

![httpatomoreillycomsourcemspimages1792323](./00_Image/httpatomoreillycomsourcemspimages1792323.png)

<!-- slide -->
# 多轮优先级排序:book:P286图16-2

![httpatomoreillycomsourcemspimages1792324](./00_Image/httpatomoreillycomsourcemspimages1792324.png)

<!-- slide -->
# 根据价值、成本和风险排优先级Prioritization Based on Value, Cost, and Risk:book:P288

1. 品质功能展开法/质量功能展开法（Quality Function Deployment, QFD）及其延伸方法是一类 **明确而严谨的结构化的** 优先级分析排序方法
1. 考虑如果获得某个特定产品特性，会为客户提供什么收益；反之，会带来什么损失

## :warning:不要过度解读计算得到的不同优先级间的细微差异，半定量方法不具有数学意义上的严谨性

<!-- slide -->
# Thank You

## :ok:End of This Chapter:ok:
