---
presentation:
    theme: white.css
    width: 1200
    height: 800
    slideNumber: true
    enableSpeakerNotes: true
    transition: zoom
    transitionSpeed: default
---

<!-- slide -->
# 第19章：需求开发之外Beyond Requirements Development:book:P325

<!-- slide -->
# :book:P325案例

![httpatomoreillycomsourcemspimages1792335](./00_Image/httpatomoreillycomsourcemspimages1792335.png)

<!-- slide -->
# 本章重点和难点

1. 本章重点：
    1. 需求到项目计划、到设计和编码、到测试

<!-- slide -->
# 估算需求工作量Estimating Requirements Effort:book:P326

1. 小项目的需求工作量约占总工作量的15%~18%
1. 合适的百分比取决于项目的大小和复杂程度
1. 需求工作量以不同的方式遍布于项目中，取决于所遵循的开发生命周期模型

<!-- slide -->
# 估算需求工作量的方法

1. 凭经验判断项目应当对需求开发投入多少工作量
1. Seilevel估算方法
    1. 基于项目整体工作量的占比经验值估算： 如15%
    1. 基于Developer-BA的人数比经验值估算： 如6:1
    1. 基于项目工件活动数量估算： 如流程、用户故事、屏显、报表等

## :warning:所有估算都基于估算者当时的知识及其假设，具有很大的不确定性，获得知识或项目完成工作后应当对估算进行优化修正

<!-- slide -->
# 从需求到项目计划From Requirements to Project Plans:book:P329

1. 需求是开展项目预期工作的基础，因此，应当根据需求对项目进行估算、规划和安排日程
1. :exclamation:最重要的项目结果是满足 **业务目标**

<!-- slide -->
# 根据需求估算项目规模和项目工作量

1. 可单独测试的需求数量
1. 功能点数量
1. 故事点数量或用例点数量
1. 用户界面元素的数量、类型和复杂度
1. 实现特定需求估算的代码行数

<!-- slide -->
# 需求和排期

1. 许多项目采用“倒排时间”方式进行需求排期
1. 需求阶段提供足够的信息，可以为一个或多个建设阶段做出符合现实的计划
1. 具有不确定需求的项目，能够从增量式和迭代式开发方法中获益

<!-- slide -->
# 有效的项目排期要素

1. 产品规模： 预估的产品规模
1. 团队效率： 从历史数据得知的开发团队的生产效率
1. 任务清单： 完整实现并验证某个特征或用例的必要任务清单
1. 需求稳定： 适度稳定的需求，至少对即将开始的开发迭代周期
1. 经验

<!-- slide -->
# 从需求到设计和编码From Requirements to Designs and Code:book:P332

## 在需求和设计间并非界限分明，而是存在灰色、模糊的过渡地带

1. 架构与分配： 需求驱动着产品的架构设计，架构则影响着需求的分配
1. 软件设计： 将需求翻译成设计，再进入编码
1. 用户界面设计： UCS -> Dialog Map -> Prototype/Display-Action-Response

<!-- slide -->
# 蓝光光碟机架构:book:P333图19-3

![httpatomoreillycomsourcemspimages1792337](./00_Image/httpatomoreillycomsourcemspimages1792337.png)

<!-- slide -->
# 高保真原型:book:P335图19-4

![httpatomoreillycomsourcemspimages1792338.png](./00_Image/httpatomoreillycomsourcemspimages1792338.png.jpg)

<!-- slide -->
# 从需求到测试From Requirements to Tests:book:P446

1. 需求分析与测试珠联璧合： 好的需求工程产生更好的测试、好的测试分析产生更好的需求
1. 需求是系统测试的基础： 要对照需求文档进行产品测试，而不应对照设计或代码进行测试
1. 测试人员验证需求实现的方法： 测试->审查->演示->分析

<!-- slide -->
# 从需求到成功From Requirements to Success:book:P337

1. 软件开发项目的最终交付物是能满足客户需要和预期的解决方案
1. 力求在严谨的规范与凭空编码间取得合理的平衡，努力将构建错误产品的风险降到可接受的水平

<!-- slide -->
# Thank You

## :ok:End of This Chapter:ok:
