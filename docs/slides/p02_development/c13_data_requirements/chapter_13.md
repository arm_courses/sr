---
presentation:
    theme: white.css
    width: 1200
    height: 800
    slideNumber: true
    enableSpeakerNotes: true
    transition: zoom
    transitionSpeed: default
---

<!-- slide -->
# 第13章：明确指定数据需求Specifying Data Requirements:book:P218

<!-- slide -->
# 本章重点和难点

1. 本章重点： ERD及其元素
1. 本章难点：
    1. ERD(Entity-Relationship Diagram)和DD(Data Dictionary)
    1. CRUD(Create, Read, Update, Delete)
    1. Data Report && Dashboard Report

<!-- slide -->
# 功能需求与数据需求

1. 哪里有功能，哪里就有数据，软件的功能就是创建、修改、显示、删除、处理和使用数据
1. 数据需求指定了系统的数据，功能需求说明了数据的用途

<!-- slide -->
# 从哪里获取数据需求

1. 系统环境图的IO流： 这些IO流从高层抽象表示主要的数据元素
1. 用户提到的名词

<!-- slide -->
# 数据模型、ERD与DD:book:P218

1. ERD和DD是常用的数据模型
1. ERD描绘了数据间的高层视图，DD描述了数据的细节视图
1. 实体关系模型或实体联系模式图(ERD)是由美籍华裔计算机科学家陈品山(Peter Chen)发明，是概念数据模型的高层描述所使用的数据模型或模式图，它为表述这种实体联系模式图形式的数据模型提供了图形符号。

<!-- slide -->
# ERD的元素和符号标记法:book:P219

1. ERD用于表示一组来自问题域的逻辑信息及其间的相互联系
1. ERD的元素：
    1. 矩形： 实体，通常用一个单数名词命名
    1. 菱形： 实体间的关系，通常用关系的自然含义命名
    1. 连线及其标记： 连接实体和关系、实体（或关系）和属性，数量或符号标记表示关系的数量关系
    1. 椭圆形： 实体的属性
1. ERD的符号标记法：
    1. Peter Chen符号标记法： :book:P219
    1. James Martin符号标记法： :book:P220
    1. UML符号标记法： :book:P221

<!-- slide -->
# ERD的优点和作用

1. 优点： 人们通常就是用实体、联系和属性这3个概念来理解现实问题的，因此，ER模型比较接近人的习惯思维方式。此外，ER模型使用简单的图形符号表达系统分析员对问题域的理解，不熟悉计算机技术的用户也能理解它，因此，ER模型可以作为用户与分析员之间有效的交流工具。
1. 作用：
    1. 作为用户与BA之间的交流工具
    1. 作为数据库设计的基础

<!-- slide -->
# 数据库的概念模型、逻辑模型和物理模型

1. 概念模型： 根据需求和业务领域知识，经分析和总结后抽象提炼出的描述数据需求的模型
1. 逻辑模型： 将概念模型转换成选定的数据库模式（如层次、网状、关系、面向对象、NoSQL），逻辑模型与数据库模式有关，与具体的数据库系统无关
1. 物理模型： 将逻辑模型在具体的数据库系统上实现

<!-- slide -->
# :book:P219图13-1

![httpatomoreillycomsourcemspimages1792311](./00_Image/httpatomoreillycomsourcemspimages1792311.png)

<!-- slide -->
# :book:P220图13-2

![httpatomoreillycomsourcemspimages1792312](./00_Image/httpatomoreillycomsourcemspimages1792312.png)

<!-- slide -->
# :book:P221图13-3

![httpatomoreillycomsourcemspimages1792313](./00_Image/httpatomoreillycomsourcemspimages1792313.png)

<!-- slide -->
# :globe_with_meridians:[实体关系模型（E-R）](http://www.hacvk.com/html/230.html)

![e-r模型-实体关系模型](./00_Image/e-r模型-实体关系模型.png)

<!-- slide -->
# DD:book:P221

1. DD是数据实体的详细信息的集合
1. DD定义了应用中所用数据元素的含义、构成、数据类型、数据长度、数据格式以及允许的取值
1. DD是词汇表的补充，词汇表定义了应用领域或业务中出现的词语、术语缩写和简称
1. DD的信息表示应用领域的数据元素以及数据结构，是设计数据库时的数据来源
1. DD经常在不同的应用间重用，特别是同一个产品线的应用间

<!-- slide -->
# DD的元素:book:P222

1. 数据元素（数据元素名称）
1. 数据描述
1. 数据类型
1. 数据长度
1. 数据取值

<!-- slide -->
# DD的数据类型及其符号标记:book:P222、图13-4

1. DD的数据类型：
    1. Primitive原始类型： 不会分解或没必要分解的数据元素
    1. Structure结构类型： 有多个数据元素构成
1. DD的符号标记：
    1. **`+`前缀：** 表示该子元素为结构类型
    1. **`()`：** 表示该子元素为可选
    1. **`MIN:MAX{}`：** 表示数组

:point_right:出现在数据类型中的数据元素必须在DD中有定义

<!-- slide -->
# DD的使用建议

1. 以字母排序
1. 使用超链接

<!-- slide -->
# 数据分析与CRUD矩阵:book:P224

1. 识别错误、细化需求： 将不同的数据模型相互参照使用，发现差异、错误以及不一致的地方
1. CRUD矩阵： 一种严格的数据分析技术，用于检测遗漏的需求
1. CRUD将行为和数据进行关联，描绘数据如何被用例Create、Read、Update和Delete
    1. 列标题： 用例
    1. 行标题： 实体

<!-- slide -->
# :book:P225图13-5

![httpatomoreillycomsourcemspimages1792314](./00_Image/httpatomoreillycomsourcemspimages1792314.png)

<!-- slide -->
# 数据报表:book:P225

1. 报表的来源： 数据库、文件或其他信息源
1. 报表的形式： 传统表格、示意图、曲线图或复合图表等
1. 报表的更新： 动态报表还是静态报表

<!-- slide -->
# 仪表盘报表:book:P230

1. Dashboard包含多个文本或图表，提供一个统一的、多维度的视图
1. Dashboard用于屏显或报印

<!-- slide -->
# :book:P230图13-7

![httpatomoreillycomsourcemspimages1792315.png](./00_Image/httpatomoreillycomsourcemspimages1792315.png.jpg)

<!-- slide -->
# Thank You

## :ok:End of This Chapter:ok:
