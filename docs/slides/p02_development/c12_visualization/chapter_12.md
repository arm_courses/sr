---
presentation:
    theme: white.css
    width: 1200
    height: 800
    slideNumber: true
    enableSpeakerNotes: true
    transition: zoom
    transitionSpeed: default
---

<!-- slide -->
# 第12章：一图胜千言A Picture is Worth 1024 Words:book:P196

<!-- slide -->
# :book:P196案例

1. 冗长的自然语言描述较难阅读和理解
1. 自然语言描述较容易产生矛盾和歧义

<!-- slide -->
# 本章重点和难点

1. 本章重点：
    1. 可视化模型的目的和适用场景
    1. DFD、流程图和泳道图、STD
1. 本章难点： 各种可视化模型的适用场景和绘制方法（DFD、流程图和泳道图、STD和状态表、对话图、决策表和决策树、事件-响应表）

<!-- slide -->
# 需求视图

1. 需求视图包括： 功能需求清单、图表、可视化模型、用户界面原型、验收测试、决策树、决策表、照片、视频、数学表达式等等
1. 没有哪一种 **单一的需求视图** 能提供全面的理解能力
1. 对于 **某些** 类型的信息，图示沟通比文字沟通更有效
    1. 对于 **某些** 类型的信息，图示的歧义更小
    1. 图示可以 **一定程度上** 跨越语言和词汇的障碍

<!-- slide -->
# 选择准确的可视化模型和建模工具:book:197

1. 不存在包罗万象的图表，单一的图示对整个系统建模没有什么意义
1. 模型应该增强而不是替换自然语言描述
1. 相比采用标准的符号表示，采用自己臆想的符号会带来更大误解风险
1. 有些模型同时适合分析和设计，取决于使用的时间和目的
1. 可视化模型能帮助识别被遗漏的、不相关的和不一致的需求
1. 专业化的建模工具的优点： 模型迭代、_约束规则_、_从需求跟踪到模型_，**`IBM Rational Rose`**、**`MS VSTS`**、**`SAP Power Designer`**

<!-- slide -->
# 从客户需求到分析模型:book:P199

:point_right::book:P199表12-1

:point_right:产品代言人（干系人）是主要的目标读者，要向他们解释每个模型的目的以及符号表示的含义

<!-- slide -->
# 选择正确的表达方式:book:P199

1. 建模必要的模型： 很少有需要为整个系统创建一套完整的分析模型，建模时关注的重点是系统中最重要、最复杂、最具风险、最具不确定性或最具歧义性的部分
1. 选择合适的模型： 精心选择合适的模型，并综合运用、比较分析它们来帮助确保这些模型的完整性（完备性）

## :point_right::book:P200表12-2

<!-- slide -->
# 数据流图（Data Flow Diagram，DFD）:book:P201

1. DFD的定义： 一种描述加工处理、数据存储、外部实体及其数据流的分析模型，刻画了数据在业务过程或软件系统中流动的行为
1. DFD的作用： 标识系统中的 **加工处理**、系统所操作的 **数据集合（存储）或物理介质** 以及在处理、存储和系统外部之间的 **数据流** ==> **IPSO（Input - Process - Storage - Output）**
1. DFD适用于 **事务处理系统（transaction-processing systems）** 和 **功能密集型应用（function-intensive application）**
1. DFD展示了 **数据流经系统的全貌**，这是其他模型难以做到的
1. UCS或Swimlane可以作为对DFD中的流程步骤细节的补充模型，DFD中的数据、外部实体应该在ERD（Entity-Relationship Diagram）中进行建模、数据字典中描述

<!-- slide -->
# DFD的元素和分层

1. DFD的元素:book:P202图12-1
    1. 圆形： 处理/工序/加工
    1. 箭头： 数据流向
    1. 双杠： 数据存储
    1. 矩形： 外部实体
1. DFD的分层:book:P202
    1. 从高层到低层： 不同的层次的DFD描绘不同的抽象层级，0层DFD描绘整体的鸟瞰图
    1. 分解的最低层： 最底层的DFD中包含基本的处理操作，这些操作可以清楚表达成一段叙述文字、一段伪代码、一个泳道图（或活动图）

<!-- slide -->
# :book:P202图12-1

![httpatomoreillycomsourcemspimages1792303](./00_Images/httpatomoreillycomsourcemspimages1792303.png)

<!-- slide -->
# DFD的一些约定:book:P203

1. 同类元素不直接通信： 工序通过数据存储进行通信，数据存储也通过工序与其他数据存储或外部实体
1. 标识与命名工序： 工序应该唯一并且具有层次性，用简洁的短语命名每个工序（处理）
1. 工序编号时序无关： 不在DFD中对工序进行时序相关的表述
1. 工序数量不应过多： 单个DFD描绘的工序数量不要超过8～10个
1. 避免只有数据流入或只有数据流出的工序

<!-- slide -->
# 泳道图:book:P204

1. 泳道图是流程图的另外一种形式，泳道图与UML中的活动图相似，有时也称为跨功能图。
1. 泳道图将子模块分解成为可视化的泳道，泳道代表在流程中执行操作的不同系统或执行者
1. 泳道图用于描述业务过程中涉及的步骤或计划开发的软件系统中的操作，常常用来描述业务过程、工作流或者是系统和用户间的交互
1. 泳道图能描述DFD中工序节点内部所发生的细节

<!-- slide -->
# 泳道图的常用元素:book:P205

1. 矩形： 流程步骤
1. 箭头： 连接流程步骤
1. 菱形： 条件判断
1. 泳道： 角色、部门或系统

<!-- slide -->
# :book:P205图12-2

![httpatomoreillycomsourcemspimages1792304](./00_Images/httpatomoreillycomsourcemspimages1792304.png)

<!-- slide -->
# 状态转换图:book:P206

1. 状态转换图（State-Transition Diagram, STD）直观表示了状态间可能的转换，UML中的State Diagram与STD类似
1. STD的元素：
    1. 矩形或圆形（一般采用圆形）： 系统状态
    1. 连线及其箭头： 状态转换
    1. 箭头文字标签： 触发状态转换的事件或行为
1. STD的终态： 只有流入的状态转换箭头而没有流出的状态转换箭头，包含一个或多个终态

<!-- slide -->
# :book:P207图12-3

![httpatomoreillycomsourcemspimages1792305](./00_Images/httpatomoreillycomsourcemspimages1792305.png)

<!-- slide -->
# 状态表:book:P208

1. 状态表（State Table）用矩阵的形式表现不同状态间可能存在的所有转换
1. 单元格表示自该行左边的状态到该列上部状态之间的转换是否有效，如有效，还需识别出导致相关的转换事件
1. STD和状态表描述相同的信息，但状态表能帮助确保所有的转换没有遗漏

<!-- slide -->
# :book:P208图12-4

![httpatomoreillycomsourcemspimages1792306](./00_Images/httpatomoreillycomsourcemspimages1792306.png)

<!-- slide -->
# 对话图:book:P209

1. 对话图（Dialog Map）在较高的抽象层次上对UI设计进行表述，它表达了系统中的对话元素及其导航连接，但没有描述界面的可视化设计
1. 对话图实际上是以STD形式建模的用户交互/用户体验（User eXperience）
1. 对话图捕获的是基本的用户-系统交互和任务流，而不纠缠于界面布局的细节
1. 需求分析阶段产生的抽象的、概念性的对话图可用于指导UI的细节设计

<!-- slide -->
# 对话图的元素

1. 矩形： 每个对话元素
1. 箭头： 转换
1. 文本标签： 动机条件

<!-- slide -->
# :book:P211图12-5

![httpatomoreillycomsourcemspimages1792307](./00_Images/httpatomoreillycomsourcemspimages1792307.png)

<!-- slide -->
# 判定表和判定树（决策表和决策树）:book:P212

1. 系统的逻辑和判定条件变得复杂后，判定表和判定树可以用来描述系统做出怎样的响应
1. 判定表列出影响系统行为的所有因素的各种取值，并表明在 **每一种因素组合** 条件下系统预期的响应动作
1. 判定树的元素：
    1. 菱形： 判断
    1. 矩形： 响应动作

<!-- slide -->
# :book:P212图12-6

![httpatomoreillycomsourcemspimages1792308](./00_Images/httpatomoreillycomsourcemspimages1792308.png)

<!-- slide -->
# :book:P213图12-7

![httpatomoreillycomsourcemspimages1792309](./00_Images/httpatomoreillycomsourcemspimages1792309.png)

<!-- slide -->
# 事件-响应表:book:P213

1. 事件-响应表（也称为事件表或事件列表）逐项记录所有事件以及作为对事件交互而产生的系统预期行为
1. 三类系统事件：
    1. 业务事件： 普通用户所执行的动作
    1. 信号事件： 系统接收到的控制信号或数据读取，或外部设备或外部系统的中断指令
    1. 时间事件： 某个时刻触发的事件
1. 事件-响应表定义事件、状态和响应的各种组合，也包括异常条件，可以作为功能需求的一部分对系统中该部分内容进行描述

<!-- slide -->
# :book:P214图12-8

![httpatomoreillycomsourcemspimages1792310](./00_Images/httpatomoreillycomsourcemspimages1792310.png)

<!-- slide -->
# UML图:book:P216

1. OOA不是必须的： 使用面向对象方法开发产品对需求开发方法并没有特别的要求，需求开发关注的是用户需要系统做什么以及系统必须具有什么功能，而不是如何实现系统
1. OOA也是有用的： 如果将以OOD技术开发系统，在需求分析时识别出“类”以及“类”的属性和行为将是有益的，因为已将问题域中的对象映射到系统中的对象和每个“类”属性和方法的细节上，将有助于从分析工作到设计工作的过渡

<!-- slide -->
# Thank You

## :ok:End of This Chapter:ok:
