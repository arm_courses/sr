---
presentation:
    theme: white.css
    width: 1200
    height: 800
    slideNumber: true
    enableSpeakerNotes: true
    transition: zoom
    transitionSpeed: default
---

<!-- slide -->
# 第18章：需求重用Requirements Reus:book:P312

<!-- slide -->
# :book:P312案例

1. **:point_right:需求重用的目标：** 提升软件开发效率
1. **需求重用的作用：** 提升生产力、改进质量、增强关联系统间的一致性
1. **需求重用的方式：**
    1. 简单方式： 从SRS中复制粘贴
    1. 复杂方式： 重用功能组件
1. 重视重用的组织
    1. **建立一些基础设施：** 使BA可以访问现有高品质的需求知识
    1. **培养重视重用的文化**

<!-- slide -->
# 本章重点和难点

1. 本章重点： 重用需求的目的？

<!-- slide -->
# 为什么要重用需求Why Reuse Requirements?:book:P313

1. 低成本、省时间、少缺陷、高效率
1. 功能一致、学习曲线低

<!-- slide -->
# 需求重用的维度Dimensions of Requirements Reuse:book:P313

<!--
TODO:
-->
:question:

<!-- slide -->
# 可以重用的需求信息类型Types of Requirements Information to Reuse:book:P316

## :book:P317表18-2

<!-- slide -->
# 常见的重用场景Common Reuse Scenarios:book:P317

1. **:point_right:软件产品线**
1. **:point_right:再设计型与替换型系统**
1. 其他： 业务过程、分布式部署、接口与集成、安全、通用的应用特性、多平台的同类产品、标准、规章及法规

<!-- slide -->
# 需求模式Requirement Patterns:book:P319

1. 需求模式（需求知识模板）： 将特定类型需求需要了解的知识进行打包，以便BA在定义这类需求时有更简单的方式
1. 需求模式通常包含： 指南、内容、模板、示例、额外要求、对开发和测试的考虑

<!-- slide -->
# 促进需求重用Tools to Facilitate Reuse:book:P319

## 使用需求管理工具

<!-- slide -->
# 使需求可重用Making Requirements Reusable:book:P320

## 采取措施使需求可重用

:warning:重用（reuse）可以节省成本，使需求变得可重用（reusable）则需要消耗成本

<!-- slide -->
# 需求重用的障碍与成功因素Requirements Reuse Barriers and Success Factors:book:P322

<!-- slide -->
# 需求重用的障碍

1. 缺少需求或需求碎片化
1. 需求NIH（Not Invented Here）和NAH（Not Applicable Here）
1. 文档写作风格
1. 文档组织方式不一致
1. 项目类型不同
1. 知识所有权归属

<!-- slide -->
# 需求重用的成功因素

1. 知识库
1. 需求质量
1. 交互： 文档链接，快捷地查找需求的相关信息
1. 术语表和数据字典
1. 组织文化

<!-- slide -->
# Thank You

## :ok:End of This Chapter:ok:
