---
presentation:
    theme: white.css
    width: 1200
    height: 800
    slideNumber: true
    enableSpeakerNotes: true
    transition: zoom
    transitionSpeed: default
---

<!-- slide -->
# 第10章：记录需求Documenting the Requirements:book:P160

<!-- slide -->
# :book:P160案例

1. 信息罗列： 将头脑风暴讨论的结果写成一个100页的文件
1. 清晰有效： BA应选择有效的方式将相应的需求传达给相应的受众，清晰和有效的沟通是需求管理的核心原则

<!-- slide -->
# 本章重点和难点

1. 本章重点：
    1. SRS的目的、结构、内容和模板
    1. 展示软件需求的主要方式：自然语言、可视化模型、形式化语言

<!-- slide -->
# 记录需求:book:P160

1. 需求开发的结果： 各方干系人对所要开发的产品达成一个协议文档 ==> VSD、UC/US、SRS
1. SRS的交付对象： 设计、开发和验证解决方案等人员

:point_right:规范说明和建模有助于从全局考虑并准确陈述重要事宜，避免仅口头的讨论所引发的歧义

<!-- slide -->
# （基础的）文件管理的局限性:book:P160

1. 内容间的相关性
1. 版本变更和控制
1. 需求分发和跟踪
1. 冗余和一致性维护

:point_right:引入Wiki, DB, RM等工具

<!-- slide -->
# 展示软件需求的方式:book:P160

1. 结构化的自然语言： 结构良好、精心写就的自然语言
1. 增强的可视化模型： 阐述过程、状态、数据关系、逻辑流等内容的可视化模型
1. 精确的形式化语言： 数学般精确的规范语言

>形式化方法（Formal Methods），也称形式方法、正规方法。在计算机科学和软件工程领域，形式化方法是基于数学的特种技术，适合于软件和硬件系统的描述、开发和验证
>
>----[Wikipedia: 形式化方法](https://zh.wikipedia.org/wiki/%E5%BD%A2%E5%BC%8F%E5%8C%96%E6%96%B9%E6%B3%95)

<!-- slide -->
# SRS和SRS的近义词:book:P162

1. SRS阐述软件系统必须具备的功能、性能、特征和必须遵循的约束
1. SRS应包括：
    1. 系统在各种条件下的行为
    1. 预期的系统属性，诸如运行状况、安全性和易用性
1. SRS不应包括：除已知的设计和执行约束外的设计、构建、测试或项目管理等方面
1. SRS是项目规划、设计、编码、测试和用户文档等后续开展产品开发和运营的基础
1. SRS的近义词： 功能规范说明Functional Specification，产品规范说明Product Specification，系统规范说明System specification，需求文档Requirements Document

<!-- slide -->
# 标识需求:book:P164

1. 每个需求都需要一个唯一的和持久的标识（标识ID）
1. 标识ID的方式：
    1. 序列号： 前缀+顺序号，不能在逻辑或层次上对需求和标识ID产生关联
    1. 层次化编号： 章节层次编号，序列号可能会变化
    1. 层次化文本标签： 以某个分隔符层次组织的文本

<!-- slide -->
# 用户界面和SRS:book:P167

1. 好处： 用户和开发人员对需求的感觉更真实
1. 坏处： 用户界面架构描绘的是解决方案，可能并非真正的需求
1. 折中的平衡方法： 将草图（低保真原型）纳入需求，该草图并不是承诺的界面设计

<!-- slide -->
# 草图（低保真原型）:book:P168图10-1

![httpatomoreillycomsourcemspimages1792301.png](./00_Images/httpatomoreillycomsourcemspimages1792301.png.jpg)

<!-- slide -->
# SRS模板:book:P168

![httpatomoreillycomsourcemspimages1792302](./00_Images/httpatomoreillycomsourcemspimages1792302.png)

<!-- slide -->
# Thank You

## :ok:End of This Chapter:ok:
