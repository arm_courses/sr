---
presentation:
    theme: white.css
    width: 1200
    height: 800
    slideNumber: true
    enableSpeakerNotes: true
    transition: zoom
    transitionSpeed: default
---

<!-- slide -->
# 第08章：理解用户需求Understanding User Requirements:book:P127

<!-- slide -->
# :book:P127案例

她用写字板纸和便签收集关于前置条件、后置条件、用户与系统间交互的信息

<!-- slide -->
# 本章重点和难点

1. 本章重点：
    1. 用户需求探索技术： **用例Use Case** 和用户故事User Story
    1. 用例图的元素和用例规约的要素
    1. 制作用例、获取用例、验证用例、用例与功能需求
1. 本章难点： 用例和用户故事的区别和联系

<!-- slide -->
# 用例和用户故事:book:P128

1. 用例Use Case：Ivar Jacobson在爱立信于1980年代中期提出，1990年代后期随着UML、RUP的兴起而传入国内
1. 用户故事User Story： XP（eXtreme Programming）之父Kent Beck于1990年代中期提出，2001年后，随着“敏捷运动1.0”的兴起逐渐火热，后续又经Scrum不断推广，2010后几年在国内兴起

<!-- slide -->
# 用例Use Case:book:P128

1. 用例： 描述 **系统** 和 **参与者** 之间的一系列交互的一种分析模型，参与者能由此一系列交互中获取价值
1. 用例是一种通过 **场景（scenarios）** 来获取需求的技术，每个用例提供一个或多个场景，该场景说明了系统如何和参与者进行互动
1. 用例名通常使用 **动宾短语** 形式:book:P129表9-1

:warning: 用例中的 **Actor** 一般翻译成“参与者”

<!-- slide -->
# 用例和使用场景Use Cases and Usage Scenarios:book:P133

1. 用例可能包含有共同目标的若干相关活动
1. 场景（使用流程）描述的是系统使用方法
1. 用例是相关使用场景的一个集合，场景是用例的特定实例
1. 在探索用户需求时，可从一个通用的用例开始开发更多具体的使用场景，也可以从一个特定场景示例归纳出整个用例

<!-- slide -->
# 什么是建模Modeling:question:

>建模就是 **建立模型**，就是为了理解事物而对事物做出的一种 **抽象**，是对事物的一种 **无歧义的书面描述**。
>
>建立系统模型的过程，又称模型化。建模是研究系统的重要手段和前提。凡是 **用模型描述** 系统的 **因果关系** 或 **相互关系** 的过程都属于建模。
>
>----[百度百科：`建模`](https://baike.baidu.com/item/%E5%BB%BA%E6%A8%A1)

<!-- slide -->
# 什么是建模Modeling:question:（续）

>Scientific modeling, the generation of a **physical**, **conceptual**, or **mathematical** representation of **a real phenomenon** that is difficult to observe directly. Scientific models are used to explain and predict the behaviour of real objects or systems and are used in a variety of scientific disciplines, **ranging from physics and chemistry to ecology and the Earth sciences**.
>
>----[Encyclopedia Britannica:  `Scientific modeling`](https://www.britannica.com/science/scientific-modeling)

<!-- slide -->
# 建模的内涵和外延

1. 建模是指通过 **变换映射** 成为 另一个 **结构化模型** 的过程，建模的目的是为了从 **另一个维度（空间）理解事物**
1. 建模包括： 数学建模、可视化建模、物理模型建模等

<!-- slide -->
# 以产品为中心 VS 以使用为中心:book:P127

1. 以产品为中心： 关注定义软件的特性实现以吸引 **潜在客户**
1. 以使用为中心： 关注 **真实用户** 及其预期用途以展现必备功能、避免无人使用特性，以确定优先级

:point_right:设计符合用户需求的软件，一个必要前提是了解用户打算用它做什么

:point_right:以使用为中心的观点逐步形成以用例方法建模需求，进而形成用例驱动模式

<!-- slide -->
# 用例驱动

@import "./00_Diagrams/uc_driven.gv"

<!-- slide -->
# 用例和用户故事（用例驱动）的适合类型:book:P128

1. 适合业务系统和用户交互系统： 业务应用程序、网站、自助终端、操作硬件等
1. 不适合缺少用例的系统： 批处理、计算密集型、业务分析、自动洗车系统等

<!-- slide -->
# 用户故事User Story:book:P128

>Extreme programming (XP) introduced the practice of expressing requirements in the form of user stories, **short descriptions of functionality-told from the perspective of a user** -that are valuable to either a user of the software or the customer of the software.
>----Mike Cohn

1. 用户故事： 一个从迫切需要该功能的人角度出发的一个短小且简单的描述
1. 用户故事的一般模板：:book:P129表8-2

```c{.line-numbers}
作为<用户类型/用户角色>，我想要<目标/期望>，以便于<原因/价值>
As a <type/role>, I want <goal/desire> so that <reason/benefit>
As a chemist, I want to request a chemical so that I can perform experiments
作为一个药剂师，我想要申请化学品，以便我可以做实验
```

<!-- slide -->
# 用例 VS 用户故事:book:P130图8-1

![httpatomoreillycomsourcemspimages1792289](./00_Images/httpatomoreillycomsourcemspimages1792289.png)

<!-- slide -->
# 用例 VS 用户故事（续）:book:P131

1. 用户故事简要说明了用户需求，用例进一步描述用户如何想象自己与系统的交互以达成目标
1. 用户故事提供简单和简洁的优势，用例为项目参与者提供用户故事所缺乏的结构和上下文
1. 有时一个用户故事与一个完整用例覆盖相同的范围，有时只表示用例的一个场景或可选流程:book:P135

:point_right:对于大型或更苛刻的项目，并非每个人都相信用户故事是一个合适的解决方案

<!-- slide -->
# 用例 VS 用户故事（再续）

>First, since a use case brief must still cover the same scope as a use case, the scope of a use case brief is usually larger than the scope of a user story. That is, **one use case brief will typically tell more than one story**.
>
>Second, use case briefs are intended to **live on for the life of a product**. User stories, on the other hand, are **discarded after use**.
>
>Finally, use cases are generally written as **the result of an analysis activity**, while user stories are written as notes that can be used to **initiate analysis conversations**.
>----Mike Cohn

<!-- slide -->
# 用例方法:book:P131

1. Actor指与系统交互执行某个用例的人（或软件或硬件）
1. 无论哪一种用户成员都可能扮演一种Actor，用户是真实的人（或系统），而Actor是抽象的:book:P132案例
1. 使用用例的方法来描述需求的过程叫 **用例建模**，用例模型主要包括以下两部分内容：
    1. 用例图Use Case Diagram： **一组用例** 的概要性的可视化模型
    1. 用例规约Use Case Specification： 对 **一个用例** 进行细致的结构化描述
确定系统中所包含的参与者、用例和两者之间的对应关系，用例图描述的是关于系统功能的一个概述。

<!-- slide -->
# 用例图案例:book:P132

![httpatomoreillycomsourcemspimages1792290](./00_Images/httpatomoreillycomsourcemspimages1792290.png)

<!-- slide -->
# 用例图 vs 关联图Context Diagram:book:P132

1. 相同点： 两者都定义了外部系统对象和内部系统事务的范围边界
1. 不同点：
    1. 关联图不提供系统内部可见性
    1. 关联图的箭头表示流动方向，用例图的箭头只表明Actor与用例间的联系，并不代表任何形式的流动

<!-- slide -->
# 用例模板及用例基本要素:book:P133

1. 用例描述（用例规约/用例说明）模板提醒每个用例应该考虑的所有信息
1. 用例的基本要素：
    1. **ID和名称：** 唯一的 **ID** 和简洁的 **名称** （指明用户目标）
    1. **文字说明：** 简短的、用于描述用例的意图
    1. **文档信息：** 创建人、创建时间、版本等信息
    1. **用例主体：**
        1. Actor参与者
        1. 三个条件： 触发条件、前置条件、后置条件
        1. 三个流程： 正常流程、可选流程、异常流程
    1. **用例信息：** 优先级、使用频率、商业规则、假定情况等
    1. **其他信息：** 记录相关性能、其他质量需求、约束和外部接口知识等不适合放在模板中任何位置的关于用例的额外信息

<!-- slide -->
# 用例模板:book:P134图8-3

![httpatomoreillycomsourcemspimages1792291](./00_Images/httpatomoreillycomsourcemspimages1792291.png)

<!-- slide -->
# 触发事件、前置条件、后置条件:book:P134

1. **触发条件：** 或称触发事件，触发系统执行某个特定用例的事件或条件
1. **前置条件：** **触发后**、**开始执行用例前** 必须满足的先决条件
1. **后置条件：** 用例执行成功后的系统状态

:warning:用户不太可能意识到用例的所有前置条件和后置条件，因此，BA可能需要从其他来源获取并分析得到关于前置条件和后置条件的输入

<!-- slide -->
# 正常流程、可选流程和异常:book:P135

1. 有编号的 **步骤列表**，展示Actor与系统间的交互顺序（a dialog that leads from the pre-conditions to the post-conditions）
    1. **正常流程Normal Flows：** 或称为主场景、主要流程、基本流程等，指正常并成功执行的流程
    1. **可选流程Alternative Flows：** 其他成功的场景，描述一些不常见（或低优先级转变）的详细说明（或如何完成的过程）
    1. **异常（流程）Exceptions：** 执行用例期间可预期的错误条件及其对应的处理方法所处的流程
1. :warning:系统崩溃一定不会出现在用户的需求列表上（系统崩溃不是业务异常）
1. :exclamation:不一定要马上实现每一个用例的可选流程，但必须解决阻止流程正常进行的异常

<!-- slide -->
# UML活动图:book:P137

![httpatomoreillycomsourcemspimages1792292](./00_Images/httpatomoreillycomsourcemspimages1792292.png)

<!-- slide -->
# 用例间的关系:book:P137

1. **扩展extend：** **有条件转移**，常用于表达扩展正常流程到可选流程
1. **包含Include：** 用于表达 **用例复用** 或 **粒度细化**，分解成用于多个用例共用的子用例，或分解成粒度更小的子用例以便后续评估、验证和估算等
1. **泛化Generalization：** 用表达 **一般** 与 **特殊** 的关系，**特殊** 用例是一个基于 **一般** 用例的独立用例

<!-- slide -->
# 扩展:book:P138图8-5

![httpatomoreillycomsourcemspimages1792293](./00_Images/httpatomoreillycomsourcemspimages1792293.png)

<!-- slide -->
# 包含:book:P138图8-6

![httpatomoreillycomsourcemspimages1792294](./00_Images/httpatomoreillycomsourcemspimages1792294.png)

<!-- slide -->
# 对齐前置条件和后置条件:book:P139图8-7

![httpatomoreillycomsourcemspimages1792295](./00_Images/httpatomoreillycomsourcemspimages1792295.png)

<!-- slide -->
# 用例和业务规则:book:P139

1. 用例和业务规则是交织在一起的
1. 一些业务规则会限制Actor执行一个用例的全部或部分，例如：也许只有特定权限级别的用户才可以执行特定的可选流程

:point_right:规则可能施加于前置条件以让系统继续执行前先通过前置条件测试

<!-- slide -->
# 识别用例:book:P139

1. 首先确定Actor，然后制定系统所支持的业务过程，并为Actor和系统的交互活动定义用例
1. 创建特定的场景来说明每个业务过程，然后将场景概括为用例并识别每个用例涉及的Actor
1. 使用过程描述
1. 识别系统必须响应的外部事件，然后将这些事件关联到参与活动的Actor和特定用例
1. 使用CRUD分析来确定需要用例创建、读取、更新、删除或控制的数据实体
1. 检查关联图

<!-- slide -->
# 探索用例:book:P141

进一步探索识别出的用例的其他信息

<!-- slide -->
# 用例获取的产出:book:P142图8-8

![httpatomoreillycomsourcemspimages1792296](./00_Images/httpatomoreillycomsourcemspimages1792296.png)

<!-- slide -->
# 验证用例:book:P142

1. BA将用例和功能需求发给会议（工作坊）参与者，让他们在下一个会议（工作坊）审阅这些内容
1. 通过早期概念性测试验证是否推导得出让用户执行每个用例所需的功能

<!-- slide -->
# 用例与功能需求:book:P143

1. 软件开发人员不会实现业务需求和用户需求，他们实现的是功能需求，即具体的系统行为
1. BA应明确指定每个用例所需要的功能需求
1. 将用例功能记入文档的几种方式：
    1. 只用用例： 在每个用例说明里一一添加
    1. 使用用例和功能需求
    1. 只使用功能需求： 在SRS或需求库中同时包含用例和功能需求
    1. 用例和测试

<!-- slide -->
# 要避免的用例陷阱:book:P145

1. 用例的数量和复杂度
    1. 太多的用例
    1. 高度复杂的用例
1. 涉及设计的用例
    1. 包含系统设计的用例
    1. 包含数据定义的用例
1. 用户无法看懂的用例

<!-- slide -->
# 要避免的用例陷阱（续）

>First, use cases often lead to **a large volume of paper**, and without another suitable place to put user interface requirements, **they end up in the use cases**.
>
>Second, use case writers focus **too early on the software implementation** rather than on **business goals**.
>
>----Mike Cohn

<!-- slide -->
# “以使用为中心”的需求的好处:book:P145

1. 用户对系统有更清晰的期望
1. 有助于BA和开发人员理解用户的业务
1. 暴露早期模棱两可、含糊不清的描述
1. 从用例生成对应的测试
1. 有助于防止“孤儿功能”（看似不错，但与用户目标不相关导致无人使用的功能）
1. 有助于排定需求优先级
1. 暴露一些重要的领域对象和彼此的责任（即，有利于生成后续阶段所需的模型）

<!-- slide -->
# Thank You

## :ok:End of This Chapter:ok:
