---
presentation:
    theme: white.css
    width: 1200
    height: 800
    slideNumber: true
    enableSpeakerNotes: true
    transition: zoom
    transitionSpeed: default
---

<!-- slide -->
# 引言：软件需求分析是什么:question:

## :point_right: 过程近义词：（软件）需求分析、软件需求、（软件）需求工程

## :point_right: 角色近义词：（（业务）分析师 == 需求分析师） >= 产品经理 >= （系统分析师 == 系统分析员）

## :hand: 非角色近义词：（系统）架构师、项目经理

<!-- slide -->
# 本章重点和难点

1. 本章重点：
    1. 什么是软件需求分析？什么是需求工程？近义词和非近义词？
    1. 需求工程的位置、作用和关注点
1. 本章难点：
    1. 深入理解为什么要学习软件需求分析？
    1. 软件需求分析涉及的不同角色、与其他相关活动的关系

<!-- slide -->
# 需求工程所处的位置和作用

1. 需求工程是软件工程的一个子工程和关键一环
1. 软件需求决定着开发的目标、是开发的依据

<!-- slide -->
# 需求工程关注什么

:point_right: 从 **问题/需要** 出发，分析 **当前或未来** 可以解决问题的需求，**建模** 并 **形成和管理** **书面化的需求文档** 用于后续软件设计开发、测试、运维等其他软件生命周期活动

<!-- slide -->
# 问题域、需求集与解系统

```dot
digraph ques_req_sol{
    bgcolor=transparent;
    node[shape=box];
    rankdir=LR;

    problems -> requirement_set[label="1 : N",dir=none];
    requirement_set -> solution_system[label="1 : N",dir=none];
}
```

<!-- slide -->
# 需求工程的特点

1. 较难度量： 沟通协作的技术和艺术
1. 文理综合： 抽象建模和文字表达相结合

<!-- slide -->
# 软件项目各阶段工作量统计（经验参考值）

|阶段|工作量比重|
|--|--:|
|项目论证与风险评估|3%|
|需求开发与需求管理|15%~18%|
|概要设计与详细设计|45%|
|编码|10%|
|测试|34%|

**:warning:仅为经验参考值，并且各部分总和不等于100%**

<!-- slide -->
# 为什么要学习软件需求分析:question:

1. 软件发生缺陷的主要原因之一（40%至50%）:book:P4
1. 交付物与需求不匹配的普遍性

<!-- slide -->
# 闭门造车的需求:book:译序

1. 希望在上线的一刹那一举形成可行的商业模式
1. 假设最初的想法是正确的
1. 认为专家就在办公室里
1. 上线之前，知道这个想法的客户数量为0

<!-- slide -->
# 互联网公司角色与职责

![roles_and_jobs_in_it_crops](./00_Image/roles_and_jobs_in_it_crops.jpg)

<!-- slide -->
# 教材及其结构

## :book:《软件需求（第3版）》，清华大学出版社:book:

1. Part 01: Software Requirements: What, Why, and Who
1. Part 02: Requirements Development
1. Part 03: Requirements for Specific Project Classes
1. Part 04: Requirements Management
1. Part 05: Implementing Requirements Engineering

<!-- slide -->
# Thank You

## :ok:End of This Chapter:ok:
