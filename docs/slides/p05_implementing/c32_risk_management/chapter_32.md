---
presentation:
    theme: white.css
    width: 1200
    height: 800
    slideNumber: true
    enableSpeakerNotes: true
    transition: zoom
    transitionSpeed: default
---

<!-- slide -->
# 第32章：软件需求和风险管理Software Requirements and Risk Management:book:P472

<!-- slide -->
# :book:P472案例

1. Problem： 经常期待下一个项目顺利进行，却不以此前项目存在的问题为鉴
1. 风险：可能造成某些损失或威胁项目成功的情况（已产生不良的结果的是问题，而不是风险）
1. 典型的需求风险包括： **需求被误解**、**用户参与不足**、**不确定或不断变化的项目范围或目标**、**持续变更的需求**

<!-- slide -->
# 本章重点和难点

1. 本章重点：
    1. 风险的定义
    1. 风险管理的定义和风险管理的要素
1. 本章难点： 需求工程各阶段（各活动）的风险

<!-- slide -->
# 软件风险管理基础:book:P473

1. 除需求风险外，项目还面临其他各种风险，常见的风险来源有：
    1. 外部实体依赖风险： 过于依赖分包商或提供可复用组件的另一个项目等
    1. 项目管理风险： 估算失误、缺少项目状态能见度、人员流动等
    1. 技术风险： 技术本身的缺陷、当前技术可行性、技术环境未齐全（idea过于先进）等
    1. 知识或经验风险： 缺乏技术知识、缺乏领域知识、缺乏领域经验等

<!-- slide -->
# 风险管理的要素

1. 风险管理： 运用工具和过程将风险限制在可接受程度内的管理过程
    1. 风险评估： 为识别潜在威胁而对项目进行查验的过程
    1. 风险规避： 不做有风险的事（应对风险的一种方式）
    1. 风险控制： 将风险控制在一个可接受程度之内（大多数情况下应对风险的一种方式）

<!--
1. 风险控制： 在风险危及项目之前就加以识别、评估以及控制的过程
1. 风险管理： 将潜在问题的可能性或影响降到最低，风险管理意味着在形成危机之前将问题处理掉
-->

<!-- slide -->
# Elements of Risk Management:book:P474图32-1

![httpatomoreillycomsourcemspimages1792384](./00_Image/httpatomoreillycomsourcemspimages1792384.png)

<!-- slide -->
# 用文档记录项目风险

<!-- slide -->
# Risk Item Tracking Template:book:P475图32-2

![httpatomoreillycomsourcemspimages1792385](./00_Image/httpatomoreillycomsourcemspimages1792385.png)

<!-- slide -->
# Sample Risk Item From The CTS:book:P476图32-3

![httpatomoreillycomsourcemspimages1792386](./00_Image/httpatomoreillycomsourcemspimages1792386.png)

<!-- slide -->
# 需求相关风险Requirements-Related Risks:book:P477

<!-- slide -->
# 需求收集阶段

1. 产品愿景与项目范围
1. 需求开发所花时间
1. 客户参与
1. 需求规约的完整性和正确性
1. 创新产品的需求
1. 非功能需求的定义
1. 客户对需求的共识
1. 未陈述的需求
1. 用作需求参照物的现有产品
1. 被当作需求的解决方案
1. 业务团队与开发团队的不信任

<!-- slide -->
# 需求分析阶段

1. 需求优先级排序
1. 技术难度特性
1. 不熟悉的技术、方法、语言、工具或硬件

<!-- slide -->
# 需求规约阶段

1. 对需求的理解
1. 处理开放性的争议性问题的时间压力
1. 用词歧义
1. 需求中包含的设计

<!-- slide -->
# 需求确认阶段

1. 未经确认的需求
1. 审查熟练度

<!-- slide -->
# 需求管理阶段

1. 不断变更的需求
1. 需求变更过程
1. 未实现的需求
1. 不断扩张的需求范围

<!-- slide -->
# 风险管理是良友Risk Management is Your Friend:book:P480

1. 利用风险管理，能够使项目经理意识到可能伤及项目的情况
1. 周期性风险跟踪，可以保证项目经理能够获悉已识别风险的威胁

<!-- slide -->
# Thank You

## :ok:End of This Chapter:ok:

<!-- slide -->
# Thank You So Much

## :ok:End of This Course:ok:
