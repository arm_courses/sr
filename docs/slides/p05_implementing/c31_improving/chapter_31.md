---
presentation:
    theme: white.css
    width: 1200
    height: 800
    slideNumber: true
    enableSpeakerNotes: true
    transition: zoom
    transitionSpeed: default
---

<!-- slide -->
# Part 05: Implementing Requirements Engineering:book:P453

<!-- slide -->
# 第31章：改进需求过程Improving Your Requirements Processes:book:P455

<!-- slide -->
# 案例:book:P455

1. 不同的项目，BA的学历和经验水平大不相同，使用的需求工程方法和工具也各有不同，因为仅仅都只是他们已知的最佳方法
1. 过程改进既要使用更多对具体情况有效的方法，也要避免使用过去曾经带来麻烦的方法
    1. 避免错误： 纠正以住项目过程中缺陷所导致的问题
    1. 预测未来： 预见和预防未来项目中可能遇到的问题
    1. 改进当下： 使用比当前所用更有效、更有成果的实践

<!-- slide -->
# 本章重点和难点

1. 本章重点：
    1. 需求与项目其他过程的关系
    1. 需求与项目不同干系人的关系
    1. 软件过程改进循环（PDCA）

<!-- slide -->
# 需求如何关联到其他项目过程How Requirements Relate to Other Project Processes:book:P456

1. 优秀的软件项目中，需求位于核心位置，支持着其他技术和管理活动得以运转
1. 需求开发和管理方法的变革影响着其他项目过程，反之亦然

<!-- slide -->
# 需求与其他项目过程的关系:book:P456图31-1

![httpatomoreillycomsourcemspimages1792375](./00_Image/httpatomoreillycomsourcemspimages1792375.png)

<!-- slide -->
# 需求与不同的干系人群体Requirements and Various Stakeholder Groups:book:P457

1. 需要从项目干系人获得哪些信息和支持？
1. 需要向项目干系人提供哪些信息和帮助？

<!-- slide -->
# Requirements-Related Contributions form Various Stakeholder to the Software Development Team:book:458图31-2

![httpatomoreillycomsourcemspimages1792376](./00_Image/httpatomoreillycomsourcemspimages1792376.png)

<!-- slide -->
# 获得对变革的承诺Gaining Commitment to Change:book:P458

1. 人们不喜欢被迫离开自己的舒适区
1. 很多抵抗来源于对不确定的恐惧
1. 当人们被迫改变其工作方式时，本能的反应都是“这对我有什么好处？”
    1. 一把手工程？
    1. 先行者？

<!-- slide -->
# Some Behaviors that Indicate Management's Commitment to Excellent Requirements Processes:book:P459图31-3

![httpatomoreillycomsourcemspimages1792377](./00_Image/httpatomoreillycomsourcemspimages1792377.png)

<!-- slide -->
# 软件过程改进基础Fundamentals of Software Process Improvement:book:P460

1. 过程改进应当是渐进的、持续的
1. 只有在有动力时，人与组织才会变革
1. 过程变革应当是目标导向的
1. 将改进工作当成是一些小项目执行

<!-- slide -->
# 根因分析法Root Cause Analysis:book:P461

1. 根因分析法： 旨在帮助确定问题背后的成因，将症状与症结进行区分
1. 因果图/鱼骨图： 又因其发明者为石川馨，故也称为石川图，能够用于描述根因分析的结果

<!-- slide -->
# 因果图示例图:book:P462图31-4

![httpatomoreillycomsourcemspimages1792378](./00_Image/httpatomoreillycomsourcemspimages1792378.png)

<!-- slide -->
# 过程改进循环The Process Improvement Cycle:book:P463图31-5

![httpatomoreillycomsourcemspimages1792379](./00_Image/httpatomoreillycomsourcemspimages1792379.png)

<!-- slide -->
# PDCA

>PDCA (plan-do-check-act or plan-do-check-adjust)
>
>P: Establish objectives and processes required to deliver the desired results.
>D: Allows the plan from the previous step to be done
>C: The data and results gathered from the do phase are evaluated
>A: Where a process is improved
>
>----[Wikipedia: PDCA](https://en.wikipedia.org/wiki/PDCA)

<!-- slide -->
# The Learning Curve, An Unavoidable Aspect of Process Improvement:book:P466图31-7

![httpatomoreillycomsourcemspimages1792381](./00_Image/httpatomoreillycomsourcemspimages1792381.png)

<!-- slide -->
# 需求工程的过程资产Requirements Engineering Process Assets:book:P466

1. 为了提升需求工程过程的绩效，每个组织都需要一系列需求过程资产
1. 过程资产包括采取的活动以及活动所产生的交付物:book:P467表31-1
1. 过程资产帮助团队持续和有效地执行过程
1. 过程资产帮助参与项目的人了解应当遵循的步骤以及期望创建的工作产出物

<!-- slide -->
# Key Process Assets:book:P467 Figure 31-8

![httpatomoreillycomsourcemspimages1792382](./00_Image/httpatomoreillycomsourcemspimages1792382.png)

<!-- slide -->
# 需求开发过程资产

1. 需求开发过程： 第07章的需求收集规划
1. 需求分配程序： 第26章
1. 需求优先级排序程序： 第16章
1. 愿景和范围模板： 第05章
1. 用例（规范）模板： 第08章
1. SRS模板： 第10章
1. 需求评审检查清单： 第17章

<!-- slide -->
# 需求管理过程资产

1. 需求管理过程： 第27章
1. 需求状态跟踪程序： 第27章
1. 变更控制过程： 第28章
1. 变更控制委员会章程模板： 第28章
1. 需求变更影响分析检查清单： 第28章
1. 需求跟踪程序： 第29章

<!-- slide -->
# 目标达到了吗？Are We There Yet?:book:P469

1. 与其他旅行一样，过程改进举措应当有一个目标
1. 指标是软件项目、产品或过程的量化面
1. KPI（Key Performance Indicators）是指与目标挂钩并能显示具体目标或结果所达成的进度的指标:book:P470表31-2
    1. 过程改进本身是无意义的，寻求业务价值提升才是目标
    1. 目标不应该是无法企及、不切实际的

<!-- slide -->
# 创建需求过程改进路线图Creating A Requirements Process Improvement Road Map:book:P470

1. 不按章法行事很难获得可持续的过程改进成功
1. 为了在组织中实施需求改进实践，需要考虑制定一个路线图
1. 过程改进路线图将改进活动联系在一起，以便以最小的投资换取最大、最快的收益

<!-- slide -->
# Sample Requirements Process Improvement Road Map:book:P31-9

![httpatomoreillycomsourcemspimages1792383](./00_Image/httpatomoreillycomsourcemspimages1792383.png)

<!-- slide -->
# Thank You

## :ok:End of This Chapter:ok:
