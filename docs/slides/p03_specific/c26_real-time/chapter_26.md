---
presentation:
    theme: white.css
    width: 1200
    height: 800
    slideNumber: true
    enableSpeakerNotes: true
    transition: zoom
    transitionSpeed: default
---

<!-- slide -->
# 第26章：嵌入式和其他实时系统项目Embedded and Other Real-Time Systems Projects:book:P388

<!-- slide -->
# 本章重点和难点

1. 本章重点：
    1. 嵌入式系统的定义和特点
    1. 实时系统的定义、特点和分类
1. 本章难点：
    1. 理解嵌入式系统和实时系统的特点和挑战

<!-- slide -->
# 嵌入式系统是什么？实时系统是什么？

1. 嵌入式系统： 使用软件控制硬件的集成了软件和硬件的整体产品
1. 实时系统： 系统的正确性严格地取决于其能否在规定时间限制内做出响应
    1. 硬实时Hard Real-Time System： 严格的时间限制，否则后果严重
    1. 软实时Soft Real-Time System： 有一定的时间限制，但后果不太严重
1. 软件比硬件可塑性更强，需求导致硬件变动比软件变动更昂贵

<!-- slide -->
# 系统需求、架构和分配:book:P388

1. 系统需求规格说明书（System Requirements Specification, SyRS）： SyRS可以作为一份单独的文档，或SRS中嵌入SyRS（特别当系统的复杂度主要存在于软件中时）
1. 系统架构： 自顶向下迭代设计

<!-- slide -->
# 系统需求的分解和分配:book:P389图26-1

![httpatomoreillycomsourcemspimages1792351](./00_Image/httpatomoreillycomsourcemspimages1792351.png)

<!-- slide -->
# 实时系统建模:book:P390

<!-- slide -->
# 跑步机的环境图:book:P390图26-2

![httpatomoreillycomsourcemspimages1792352](./00_Image/httpatomoreillycomsourcemspimages1792352.png)

<!-- slide -->
# 跑步机的状态转换图:book:P391图26-3

![httpatomoreillycomsourcemspimages1792353](./00_Image/httpatomoreillycomsourcemspimages1792353.png)

<!-- slide -->
# 跑步机的事件响应表:book:P392表26-1

<!-- slide -->
# 跑步机的架构图:book:P393图26-4

![httpatomoreillycomsourcemspimages1792354](./00_Image/httpatomoreillycomsourcemspimages1792354.png)

<!-- slide -->
# 原型与仿真 - [树莓派Raspberry Pi](https://www.raspberrypi.org/)

![wide-hero-shot](./00_Image/wide-hero-shot.png)

<!-- slide -->
# [Raspberry Pi4 Model B](https://www.raspberrypi.org/products/raspberry-pi-4-model-b/)

![pi4-labelled](./00_Image/pi4-labelled.png)

<!-- slide -->
# 接口:book:P394

1. 接口是嵌入式系统和其他实时系统重要方面之一
1. 四类外部接口需求： 用户、软件、硬件、通信
1. 复杂系统拆分成多个子系统时的内部组件间接口

<!-- slide -->
# 接口规格说明模板:book:P26-5

![httpatomoreillycomsourcemspimages1792355](./00_Image/httpatomoreillycomsourcemspimages1792355.png)

<!-- slide -->
# 时序需求:book:P395

1. 时序需求是实时系统的核心
    1. 延迟时间： 触发事件发生后到系统响应所经历的时间
    1. 执行时间： 某一任务从启动到完成所经历的时间
    1. 可预测性： 重复以一致的时间间隔发生的循环事件

<!-- slide -->
# 嵌入式系统的质量属性:book:P396

1. **:point_right:质量属性需求对嵌入式和实时系统尤为关键**
1. 特别重要的质量属性包括： 性能、效率、可靠性、健壮性、安全性、保密性和可用性
1. 运行环境因素： 嵌入式系统的运行环境包括极端气候、震动、冲击等各种考验因素
1. 只适用于硬件系统的质量属性和限制： 尺寸、形状、重量、材料、可燃性、连接器、持久性、成本、噪音级别、强度等

<!-- slide -->
# 嵌入式系统的挑战:book:P400

1. 更需要采用系统工程的方式
1. 架构和设计选择与需求分析更加密切相关
1. 对于约束和质量属性关注点与纯软件系统不同，并且常常与OS的考虑交织在一起
1. 系统需求、软件需求、硬件需求、接口需求

<!-- slide -->
# Thank You

## :ok:End of This Chapter:ok:
