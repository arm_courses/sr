---
presentation:
    theme: white.css
    width: 1200
    height: 800
    slideNumber: true
    enableSpeakerNotes: true
    transition: zoom
    transitionSpeed: default
---

<!-- slide -->
# 第25章：业务分析项目Business Analytics Projects:book:378

<!-- slide -->
# 本章重点和难点

1. 本章重点： BI是什么？
1. 本章难点： BI和BD的联系和区别

<!-- slide -->
# 业务分析项目是什么？

1. 业务分析Business Analytics： 也称为 **商业智能Business Intelligence** 或 **商业报表Business Reporting**，主要目的是将数量庞大且高度复杂的数据集合转换成可供决策使用的有用信息
1. BI可以在战略、战役或战术层面进行辅助决策

<!-- slide -->
# BI框架的简要构成:book:P379图25-1

![httpatomoreillycomsourcemspimages1792348](./00_Image/httpatomoreillycomsourcemspimages1792348.png)

<!-- slide -->
# 业务分析项目概述:book:P378

1. 报表是BI的核心功能
1. 从描述性分析Descriptive Analytics到预测性分析Predictive Analytics
1. 考虑增量式开发BI

<!-- slide -->
# 分析类型:book:P379图25-2

![httpatomoreillycomsourcemspimages1792349](./00_Image/httpatomoreillycomsourcemspimages1792349.png)

<!-- slide -->
# 业务分析项目的需求开发:book:P380&&:book:P381图25-3

![httpatomoreillycomsourcemspimages1792350](./00_Image/httpatomoreillycomsourcemspimages1792350.png)

<!-- slide -->
# 分析的演进本质:book:P386

1. 理想的情况：用户有一个问题，然后请求一份报表，该报表包含与决策相关的信息
1. 现实的情况：用户看到信息后，进一步思考分析问题，进而请求一份新的报表

<!-- slide -->
# BI vs BD

||BI|BD|
|--|--:|--:|
|数据来源|业务数据|行为数据为主|
|数据类型|结构化数据|非结构化数据为主|
|计算形式|离线计算|在线计算为主|
|分析模型|基于确定性模型|基于不确定性模型|
|分析类型|描述型为主|预测型为主|
|分析结果|辅助决策|自动决策|

<!-- slide -->
# Thank You

## :ok:End of This Chapter:ok:
