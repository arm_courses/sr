---
presentation:
    theme: white.css
    width: 1200
    height: 800
    slideNumber: true
    enableSpeakerNotes: true
    transition: zoom
    transitionSpeed: default
---

<!-- slide -->
# 第20章：敏捷项目Agile Projects:book:P341

<!-- slide -->
# 本章重点和难点

1. 本章重点：
    1. 敏捷模型的定义和特点
    1. 敏捷模型的Backlog

<!-- slide -->
# Build vs Construction Phase

1. **构建Build：** 将源码转换为可以运行的实际代码，比如安装依赖，配置各种资源（运行参数、样式表、JS脚本、图片等），**`Jenkins`**、**`Travis`**、**`GitLab CI`**
1. **构建阶段Construction Phase：** 将设计转化为实现，并进行集成和测试

<!-- slide -->
# Continuous Integration

![continuous_integration](./00_Image/continuous_integration.png)

<!-- slide -->
# Continuous Delivery && Continuous Deployment

![continuous-delivery-deployment-sm](./00_Image/continuous-delivery-deployment-sm.jpg)

<!-- slide -->
# 敏捷开发概述:book:P341

1. 敏捷方法基于 **迭代式** 和 **增量式** 软件开发方法发展而来，是一套鼓励干系人间持续合作并快速、频繁地以 **小增量** 的方式交付有用功能的软件开发方法
1. 敏捷开发方法种类繁多，流行的包括： Scrum，极限编程eXtreme Programming，精益软件开发方法Lean Software Development，特征驱动开发Feature-Driven Development，看板方法Kanban等
1. 各种敏捷方法各有千秋，本质上都离不开 **适应型Adaptive/变化驱动Change-Driven**，而不是预测型Predictive/计划驱动Plan-Driven
1. 敏捷方法等适应型方法旨在适应项目中发生的不可避免的变化，它们对 **需求高度不确定或波动** 的项目也很有效

<!-- slide -->
# 瀑布模型的局限性Limitations of The Waterfall:book:P341

1. 通常认为，瀑布模型中的各项活动（各个阶段）是一个 **线性序列** ，为了尽量使整个需求集合“正确”，团队会投入大量的精力，事实上，**几乎没有项目会使用完全串行的瀑布模型** ，在各阶段间总会有某些重叠和反馈
1. 预测型项目会预判 **有限的变化** 并采取相应的处理措施
1. 从 **完全固定的、预测型的项目** 到 **完全不确定、适应型的项目** 之间的范围中，**关键区别在于从某个需求创建到基于这个需求的软件交付给客户所经过时间的长短**
1. 在周期长的项目中，干系人经常改变需求，导致基于瀑布模型的大型项目通常会延期交付、缺少必要的特性并且不能满足用户的期望

<!-- slide -->
# 敏捷开发方法:book:P343

1. 敏捷方法试图解决瀑布模型的局限
1. 敏捷方法专注于 **迭代** 和 **增量** 开发方式，将软件开发周期分解成多个称为“迭代”的短周期（Scrum中称为“冲刺Sprints”），每个迭代周期 **短至一周甚至一天**、**长到一月**
1. 后续的增量对已有产品进行修正： 完善原有特性、加入新特性或纠正已发现的缺陷
1. **持续的客户参与：** 尽早发现问题和调整方向，从而指导开发人员调整其工作过程
1. **快速的产品交付：** 目标在每个迭代周期的最后产出潜在可交付的软件，即使只是预期最终产品的一小部分

## Nightly Build/Insiders Build ==> For early adopters, you can get the latest release of VS Code each day with the Insiders Build

<!-- slide -->
# 敏捷方法中的需求基本面:book:P343

<!-- slide -->
# 客户参与

1. 瀑布模型： 软件分析、测试阶段有客户参与，构建阶段一般没有客户参与
1. 敏捷项目： 客户持续参与整个项目

<!-- slide -->
# 文档细节

1. 瀑布模型： 详细说明的文档
1. 敏捷项目： 基本够用的文档，更依赖BA与客户的持续沟通

## 开源项目一般采用在线论坛形式沟通

<!-- slide -->
# Backlog和优先级排序

1. Backlog： 包含团队采取行动的需求清单， **每个项目应只维护一个Backlog**
1. 优先级排序： 持续的排序过程

<!-- slide -->
# 时间控制:book:P345图20-1

![httpatomoreillycomsourcemspimages1792339](./00_Image/httpatomoreillycomsourcemspimages1792339.png)

<!-- slide -->
# 史诗、用户故事和特性

1. 用户故事US： 一段简要说明，用以阐明用户的需要并以此为起点进行沟通，**用户故事的粗度要求能在一个迭代周期内完全实现**
1. 史诗Epic： 规模过大以至无法在一个迭代周期内完全实现的用户故事
1. 特性Feature： 能为用户提供价值的能力集合，特性可以包含单个或多个用户故事、单个或多个史诗

## ==最小适销特性Minimum Marketable Feature==：以业务需求为目标，识别出最底层的故事，以便能确定团队能交付给客户并为其提供价值的最小功能集合

<!-- slide -->
# 故事分解:book:P345图20-2

![httpatomoreillycomsourcemspimages1792340](./00_Image/httpatomoreillycomsourcemspimages1792340.png)

<!-- slide -->
# 期待变更

1. 态度转变： not "Wait, that's out of scope" or "We need to go through a formal process to incorporate that change," but rather, "Okay, let's talk about the change"
1. 关注未来： 创造一套更加可扩展和健壮的架构或设计，以便日后加入新的功能

<!-- slide -->
# 根据敏捷项目调整需求实践:book:P347

## 传统的实践通过调整 ==应用的时机==、==应用的程度== 或者 ==执行实践的人== 都可以容易地应用到敏捷项目

<!-- slide -->
# 敏捷转型，怎么办？:book:P347

1. 确定在团队中的角色
1. 充分理解敏捷项目的概念和角色： 产品负责人、用户故事、验收测试、Backlog等
1. 确定敏捷实践是否适合自己的组织
1. **:white_check_mark:在一个小项目或项目的一小部分中试点敏捷方法**
1. 经验丰富的教练指导进行敏捷的起步
1. **:white_check_mark:不为敏捷而敏捷**

<!-- slide -->
# Thank You

## :ok:End of This Chapter:ok:
