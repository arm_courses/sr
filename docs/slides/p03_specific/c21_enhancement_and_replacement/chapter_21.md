---
presentation:
    theme: white.css
    width: 1200
    height: 800
    slideNumber: true
    enableSpeakerNotes: true
    transition: zoom
    transitionSpeed: default
---

<!-- slide -->
# 第21章：改进型和替换型项目Enhancement and Replacement Projects:book:P349

<!-- slide -->
# 本章重点和难点

1. 本章重点：
    1. 改进型项目和替换型项目的定义和区别
    1. 改进型项目和替换型项目的独特挑战： 文档缺失和用户抵制

<!-- slide -->
# 改进型项目和替换型项目是什么

1. 零起点项目A Green-Field Project： 新软件系统的开发项目
1. 改进型项目An Enhancement Project： 向现有系统添加新功能、改进功能、纠正缺陷的项目
1. 替换型项目A Replacement (Re-engineering) Project： 使用新系统替换现有系统的项目

## :exclamation:The Case of The Missing Spec:book:P349

<!-- slide -->
# 预期的挑战:book:P350

1. 新的变化导致系统性能降低
1. **:exclamation:现有系统缺少或没有可用的需求文档**
1. 需求文档已经过时
1. **:exclamation:用户不喜欢新的变化（被动变化）**
1. 无意间破坏或忽略重要干系人关注的功能
1. 干系人要求添加貌似必需的功能
1. 新系统的学曲线扰乱当前的运营

<!-- slide -->
# 基于现有系统的需求技术:book:P350表21-1

<!-- slide -->
# 按业务目标进行优先级排序:book:P351

1. 遏制镀金带来的风险： 替换型项目主要专注于对现有功能进行移植
1. 找出替换型项目无需保留的现有功能

<!-- slide -->
# 留心差异

## 差异分析： 对现有系统和新系统的功能进行对比 => 为确保理解了为何当前系统无法满足当前的业务目标

![httpatomoreillycomsourcemspimages1792341](./00_Image/httpatomoreillycomsourcemspimages1792341.png)

<!-- slide -->
# 维持性能水平

1. 现有系统决定着用户对性能和吞吐量的（最低）期望
1. 优先考虑维持最重要的KPI

<!-- slide -->
# 找不到原有需求怎么办？:book:P353

1. 软件考古学： 对用户界面、代码和数据库进行逆向工程
1. 将零散的知识表述不断地延展积累，使系统文档得以持续稳步的改进

<!-- slide -->
# 应当指定哪些需求？

1. 确定需要记录的需求区域
1. 确定需要记录的详细程度
1. 尝试建立跟踪链

<!-- slide -->
# 向缺乏文档的现有系统增加能见度:book:P354图21-2

![httpatomoreillycomsourcemspimages1792342.png](./00_Image/httpatomoreillycomsourcemspimages1792342.png.jpg)

<!-- slide -->
# 如何发现现有系统的需求

## 在改进型项目和替换型项目中，无论有无现成文档，都需要一个用于发现相关需求的工作体系

<!-- slide -->
# 鼓励使用新系统:book:P356

1. BA、项目经理或项目发起人，必须预见用户抗拒新系统带来的阻力并计划如何克服以让用户更容易接受新系统
1. 数据转换、用户培训、组织和业务过程变更、新旧系统并存等
1. 减少加班？增加额外的收益？

<!-- slide -->
# 是否可以迭代:book:P357

1. 改进型项目可能可以以增量方式进行，替换型项目则较难使用增量方式
1. 改进型项目和替换型项目两大独特挑战：
    1. 文档缺失
    1. 用户抵制

<!-- slide -->
# Thank You

## :ok:End of This Chapter:ok:
