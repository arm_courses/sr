---
presentation:
    theme: white.css
    width: 1200
    height: 800
    slideNumber: true
    enableSpeakerNotes: true
    transition: zoom
    transitionSpeed: default
---

<!-- slide -->
# Part 01：Software Requirements: Wht, Why and Who:book:P1

<!-- slide -->
# 第01章：软件需求的本质The Essential Software Requirement:book:P3

<!-- slide -->
# :book:P1案例

<!-- slide -->
# 本章重点和难点

1. 本章重点：
    1. 需求的层次和种类
    1. 需求的典型交付物
    1. 需求开发的各活动
    1. 需求开发和需求管理的分界点
    1. 需求管理的各活动
1. 本章难点：
    1. 不同需求层次、不同交付物、不同活动的区别和联系
    1. 工程、过程、活动的区别和联系
    1. 需求开发和需求管理的分界点、区别和联系

<!-- slide -->
# 需求是什么:question:

:joy: 不要妄想在项目中的所有干系人都能对需求达成统一的认识

<!-- slide -->
# [The IEEE Standard Glossary of Software Engineering Terminology](https://en.wikipedia.org/wiki/Software_requirements)

>1. **A condition or capability** needed by a user to solve a problem or achieve an objective.
>1. **A condition or capability** that must be met or possessed by a system or system component to satisfy a contract, standard, specification, or other formally imposed document.
>1. **A documented representation** of **a condition or capability** as in 1 or 2.

<!-- slide -->
# 需求的层次和种类:book:P6

![httpatomoreillycomsourcemspimages1792257](./00_Image/httpatomoreillycomsourcemspimages1792257.png)

<!-- slide -->
# 业务需求、用户需求和功能需求

1. 业务需求： 描述组织（发起方）为什么要开发产品（希望获得的业务收益），其关注点在于组织有哪些业务目标==>**Why为什么要开发产品？**==>VSD/MRD
1. 用户需求： 描述用户使用产品必须完成的目标和任务，并且这个产品要能够为人提供价值，还包括对用户满意度最为关键的产品特性或特征的描述==>**What系统能做什么任务？**==>URD（用例、用户故事、事件响应表）
1. 功能需求： 描述产品在特定条件下所展示出来的行为，主要描述开发人员需要实现的功能以便用户能够完成的任务（用户需求），进而满足业务需求，最终产生价值==>**What系统的具备什么能力？**==>SRS/PRD

:point_right:用户需求的主角是用户，功能需求的主角是系统

<!-- slide -->
# 业务分析师与软件需求规范说明:book:P9

1. 业务分析师（Business Analyst，BA）将功能需求记录在SRS中，尽可能详尽地描述人们对软件系统的预期行为。
1. 软件需求规范说明书（Software Requirements Specification，SRS）的近义词：业务需求文件（Business Requirements Document，BRD）、功能规范说明（Functional Specifications Document）、需求文件（Requirements Document，RD）等

## :warning:在互联网行业，BRD（Business Requirements Document, 商业需求文档），是最初的（在VSD/MRD之前，功能相当于立项申请书）、给投资人看的文档，BRD的目标是获得投资人的投资

<!-- slide -->
# 业务规则

业务规则包括公司政策、政府法规、工业标准以及计算算法，业务规则本身不是软件需求（它的存在已超出了任何特定软件应用的范围），但业务规则有时又引申出具体的质量特性，这些特性又以某种功能的方式由开发人员实现

<!-- slide -->
# 非功能需求

## :point_right: 强调的并不是系统要做什么，其重点在于系统做得有多棒

1. 质量属性： 性能、安全性、易用性、可移植性等
1. 外部接口： 其他软件系统、硬件组件、用户以及沟通界面等
1. 约束条件： 设计约束、实现约束等
1. 运行环境： 软硬件平台、可移植性、兼容性、约束、监管、地域性需求等

:thinking:

1. 外部接口属于功能需求还是非功能需求？
1. 运行环境是否也属于一种约束条件？

<!-- slide -->
# 需求层次及对应角色:book:P12

![httpatomoreillycomsourcemspimages1792259](./00_Image/httpatomoreillycomsourcemspimages1792259.png)

<!-- slide -->
# 需求交付物:book:P12

1. 愿景和范围文档VSD
1. 用户需求文档URD
1. 软件需求规范说明SRD

<!-- slide -->
# 产品需求与项目需求:book:P13

1. SRS包含产品需求，不包括设计或执行细节、项目计划、测试计划
1. BA在收集和传播 **产品信息** 时扮演主要角色（做什么），项目经理的主要职责在于沟通 **项目信息**（哪个时间做哪些）

:point_right: 项目是为完成某一独特的产品或服务所做的 **一次性努力**

<!-- slide -->
# 需求工程是什么？

<!-- slide -->
# 工程、过程和活动

1. 工程Engineering： 偏静态术语，强调结构性、系统性
1. 过程Process： 动态术语，实现工程的一系列活动集合
1. 活动Activity： 动态术语，可明确分派到具体个人完成的任务集合

<!-- slide -->
# 需求开发和需求管理:book:P14

![httpatomoreillycomsourcemspimages1792260](./00_Image/httpatomoreillycomsourcemspimages1792260.png)

<!-- slide -->
# 需求开发和需求管理的分界点:book:P16

![httpatomoreillycomsourcemspimages1792261](./00_Image/httpatomoreillycomsourcemspimages1792261.png)

<!-- slide -->
# 软件危机是什么:question:



<!-- slide -->
# 需求风险:book:P18

1. 规划不当
1. 忽视干系人
1. 用户参与度不够
1. 用户需求蔓延
1. 需求模棱两可
1. 镀金

:exclamation:用户并不清楚自己需要什么或描述不准确，如：触屏手机、:book:P59

<!-- slide -->
# Thank You

## :ok:End of This Chapter:ok:

<!--

# 纠正缺陷花费的成本

|发现时间|纠正成本|倍数|
|--|--|--|
|需求|100 - 1,000|--|
|单元测试|> 1,000|> 10|
|系统测试|7,000 - 8,000|70 - 80|
|验收测试|10,000 - 100,000|100 - 1,000|
|发布后|> 1,000,000|> 10,000|

注：成本单位为美元，倍数指相对于需求阶段的倍数

-->
