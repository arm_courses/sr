---
presentation:
    theme: white.css
    width: 1200
    height: 800
    slideNumber: true
    enableSpeakerNotes: true
    transition: zoom
    transitionSpeed: default
---

<!-- slide -->
# 第03章：需求工程优秀实践Good Practices for Requirements Engineering:book:P38

<!-- slide -->
# :book:P38案例

## :muscle:为了面对每个项目的挑战，所有软件专家都必须有一个专业技能工具箱

<!-- slide -->
# 本章重点和难点

:book:P39表3-1

<!-- slide -->
# 迭代的需求开发:book:P40

![httpatomoreillycomsourcemspimages1792264](./00_Images/httpatomoreillycomsourcemspimages1792264.png)

<!-- slide -->
# 需求开发工作量分布

![httpatomoreillycomsourcemspimages1792266](./00_Images/httpatomoreillycomsourcemspimages1792266.png)

<!-- slide -->
# 优秀实践：需求管理:book:P47

1. 建立一个需求变更控制流程
1. 对需求变更进行影响分析
1. 建立基线并控制需求集合版本
1. 维护需求变更的历史记录
1. 跟踪每个需求的状态
1. 跟踪需求问题
1. 维护一个需求可跟踪矩阵
1. 使用需求管理工具

<!-- slide -->
# 优秀实践：知识:book:P49

1. 培训以达成共识：训练业务分析师、帮助干系人理解需求、帮助开发人员理解应用领域
1. 积累以提升效率：制定一个需求工程流程、建立词汇表

<!-- slide -->
# Thank You

## :ok:End of This Chapter:ok:
