---
presentation:
    theme: white.css
    width: 1200
    height: 800
    slideNumber: true
    enableSpeakerNotes: true
    transition: zoom
    transitionSpeed: default
---

<!-- slide -->
# 第29章：需求链中的链接Links in The Requirements Chain:book:P432

<!-- slide -->
# :book:P432案例

1. 问题Problem：软件变更看似简单，却往往有着深远的影响，需要对系统的许多部分进行修改
1. 需求Requirement： 如果有类似路线图这样的可以显示每个需求或软件实现的业务逻辑，做变更影响分析就容易得多
1. 方案Solution： 需求跟踪信息记录着某个需求与系统其他元素间的依赖和逻辑关联

<!-- slide -->
# 本章重点和难点

1. 本章重点： 需求跟踪的4种类型
1. 本章难点： 需求跟踪的具体方法

<!-- slide -->
# 需求跟踪Tracing Requirements:book:P432

1. 通过跟踪链接，可以跟踪需求从提出到实施的生命周期，实现前向和后向的跟踪
1. 可跟踪与被跟踪不是同一概念，可跟踪指的是需求具体与其他元素之间逻辑链接这一可促进跟踪的属性
1. 可跟踪的需求，必须具有唯一的、持久的标识以便可在整个项目中被无歧义地引用

<!-- slide -->
# 四种类型的需求跟踪:book:P433图29-1

![httpatomoreillycomsourcemspimages1792368](./00_Image/httpatomoreillycomsourcemspimages1792368.png)

<!-- slide -->
# 需求跟踪链接示例图:book:P434图29-2

![httpatomoreillycomsourcemspimages1792369](./00_Image/httpatomoreillycomsourcemspimages1792369.png)

<!-- slide -->
# 需求跟踪的动机Motivations for Tracing Requirements:book:P434

1. 验证合规： 需求跟踪提供一种方式来说明产品是否符合规范、合同或法规
1. 提高质量： 需求跟踪可以提高产品质量、降低维护成本并有利于重用

<!-- slide -->
# 需求跟踪的好处和用途

1. 发现遗漏的需求
1. 发现不必要的需求
1. 认证与合规
1. 变更影响分析
1. 维护
1. 项目跟踪
1. 重构
1. 复用
1. 测试

<!-- slide -->
# 需求跟踪矩阵The Requirements Traceability Matrix:book:P435

1. 需求跟踪矩阵Requirements Traceability Matrix： 一种常用的代表需求和系统其他元素之间的链接:book:P436表29-1
1. 双向跟踪矩阵:book:P437表29-2
1. :exclamation:非功能需求，往往不能直接跟踪到具体代码，可以通过跟踪非功能需求对应的功能需求或交付物达到跟踪非功能需求的目的
1. 跟踪链应该由掌握合适信息的人进行定义

<!-- slide -->
# 非功能需求（安全性）的跟踪链示例:book:P437图29-3

![httpatomoreillycomsourcemspimages1792371](./00_Image/httpatomoreillycomsourcemspimages1792371.png)

<!-- slide -->
# 需求跟踪工具Tools for Requirements Tracing:book:P438

1. 定义对象间的链接
1. 区分Traced-To和Traced-From关系
1. 链接的一端修改，自动标记该跟踪链接的状态（可疑状态？）
1. 定义跨项目或跨子系统的链接

<!-- slide -->
# 需求跟踪过程A Requirements Tracing Procedure:book:P439

1. 认识重要性
1. 定义待跟踪的链接关系
1. 选择跟踪矩阵工具
1. 确定待跟踪的信息
1. 分配责任人
1. 制定提醒机制
1. 定义ID标识符规则
1. 收集跟踪信息
1. 定期审计跟踪信息以确保最新

<!-- slide -->
# 需求跟踪可行吗？有必要吗？Is Requirements Tracing Feasible? Is It Necessary?:book:P440

1. 投产比： 只有团队可以决定需求跟踪是否能够为项目增加价值且高于投入的相应成本
1. 业务规则： 有些软件会受到相关政策约束是否应具备需求跟踪机制和活动

<!-- slide -->
# Thank You

## :ok:End of This Chapter:ok:
