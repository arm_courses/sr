---
presentation:
    theme: white.css
    width: 1200
    height: 800
    slideNumber: true
    enableSpeakerNotes: true
    transition: zoom
    transitionSpeed: default
---

<!-- slide -->
# 第30章：需求工程工具Tools for Requirements Engineering:book:P442

<!-- slide -->
# :book:P442案例

1. 问题Problem： 单纯基于文件的需求管理方式存在很多不足，难于满足需求的迭代更新、检索跟踪、同步协作等的要求
1. 工具的作用：
    1. RD工具帮助 **获取和记录项目的正确需求** 并辅助判断 **它们是否写得好**
    1. RM工具帮助管理 **需求变更**、**跟踪状态** 并 **跟踪需求与其他项目交付物的链接**
1. 流程与工具相辅相成： 工具无法替代良好的流程，良好的工具促进流程的实施

<!-- slide -->
# 本章重点和难点

1. 本章重点： RD工具的特点
1. 本章难点： 各种工具实现的目的、优缺点和选择

<!-- slide -->
# 需求开发工具Requirements Development Tools:book:P443

1. RD工具的特点：
    1. RD工具不如RM成熟（RM处理的问题更理性、更可度量）
    1. RD工具对项目的整体影响一般也不如RM工具
1. RD工具包括但不限于：
    1. 获取工具： 需求获取阶段用于记笔记的工具，促使BA快速组织想法组织想法并标注内容，如：思维导图、录音笔、相机、投票等
    1. 建模工具： 帮助创建各种模型，复杂的工具可跟踪单个具体的需求与模型（甚至到具体元素）的联系
    1. 原型工具： 可以促进形成成品的初步实现，如： WireFrame, PowerPoint, 高保真原型工具，编程语言制作的可运行演进模型

<!-- slide -->
# 需求管理工具Requirements Management Tools:book:P445

1. RM工具将信息保存到一个由 **多用户共享** 的数据库中
1. RM工具的好处：
    1. 版本管理和变更控制
    1. 保存需求属性
    1. 促进变更影响分析
    1. 识别遗漏的需求和无关的需求
    1. 跟踪需求状态
    1. 控制访问权限
    1. 同步信息
    1. 复用需求
    1. 跟踪问题状态
    1. 生成定制子集

<!-- slide -->
# RM工具的常见功能:book:P447图30-1

![httpatomoreillycomsourcemspimages1792373](./00_Image/httpatomoreillycomsourcemspimages1792373.png)

<!-- slide -->
# RM工具与其他工具的集成:book:P448图30-2

![httpatomoreillycomsourcemspimages1792374](./00_Image/httpatomoreillycomsourcemspimages1792374.png)

<!-- slide -->
# 挑选和实现需求工具Selecting and Implementing a Requirements Tool:book:P449

1. 选择工具： **BA主导** 工具的选择，并按一定的模式选择工具
1. 部署工具： 安装工具、配置设置、导入信息、集成工具、扩展工具
1. 制定流程： 可能需要调整现有流程以便适应工具、处理工具无法解决的流程问题
1. 引导使用： 购买工具简单，更改文化和流程、接受并充分利用工具则困难得多，人们往往抵制改变自己已熟悉的事物
    1. 价值宣传： 倡导工具价值
    1. 人员培训： 确定工具倡导者，培训团队成员
    1. 试用工具： 在非关键项目试用工具

<!-- slide -->
# Thank You

## :ok:End of This Chapter:ok:
