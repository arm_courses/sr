---
presentation:
    theme: white.css
    width: 1200
    height: 800
    slideNumber: true
    enableSpeakerNotes: true
    transition: zoom
    transitionSpeed: default
---

<!-- slide -->
# 第28章：需求变更Change Happens:book:P415

<!-- slide -->
# 案例:book:P415

1. 现象：私下即变更，未评估即变更
1. 结果：
    1. 看似简单的变更，可能结果变得超乎想象的复杂
    1. 不受控制的变更，可能导致陷入混乱、质量问题

<!-- slide -->
# 本章重点和难点

1. 本章重点：
    1. 变更控制的必要性和目的
    1. CCB与CCP
1. 本章难点：
    1. CCP模板

<!-- slide -->
# 为什么要管理变更Why Manage Changes:book:P415

1. 变更的必要性
    1. 变更不可避免
    1. 变更并非坏事，反而是必要的
1. 变更控制的必要性： 不受控的变更是可怕的
1. 变更控制的目的： 通过一致的变更控制流程避免不受控的变理、信息不一致等问题产生而来的各种后果

<!-- slide -->
# 管理范围蔓延Managing Scope Creep:book:P416

1. 需求增长包括在需求集合形成基线后提出的新功能和重大修改
1. 持续并入更多功能而不相应地调整资源、排期和质量目标的范围蔓延是有害的
1. 需求变更本身不是问题，问题在于变更对已开始的工作产生的巨大影响
1. 管理范围蔓延
    1. 第一步： 记录下新系统的业务目标、产品愿景、项目范围和限制（业务需求基线）
    1. 第二步： 将提出的每一个需求或功能与业务需求基线相比较评估

<!-- slide -->
# 变更控制委员会The Change Control Board:book:P422

1. CCB的主体： 个人或群体形成的组织
1. CCB的责任：
    1. 负责决定提交的变更和新需求是否接受、接受或驳回哪个版本的修订
    1. 负责决定哪些缺陷需要修复以及何时修复
1. CCB的权利
    1. 有些CCB具有决策权
    1. 有些CCB只有向管理层的建议权
    1. 有些CCB执行分层分级决策权

<!-- slide -->
# CCB的成员

1. 项目或项目集的管理层人员
1. BA或PM
1. 开发人员、测试或质量保障人员
1. 技术支持或客服人员
1. 市场人员、业务人员、客户代表

<!-- slide -->
# 变更控制策略Change Control Policy:book:P417

1. 管理层应向下传达变更政策，说明他们希望如何处理需求和其他所有重要的项目产出物的变更
1. 制定的政策现实、可以增加价值且得心强制执行时，才有意义

<!-- slide -->
# 变更控制流程的基本概念Basic Concepts of The Change Control Process :book:P418

1. 合理的CCP可以使项目负责人做出明智的决策，在控制成本和排期的同时提供最大的客户价值和业务价值
1. 合理的CCP可以用来跟踪所有变更请求的状态，并确保建议的变更不会被丢失或被忽视
1. 合理的CCP是一个漏斗和过滤机制，它并不是阻止必要的变更的障碍，而是旨在确保项目尽快采用最适当的变更
1. 合理的CCP有良好的文档、尽可能地简单，而且尽可能地高效

<!-- slide -->
# 变更控制流程描述A Change Control Process Description:book:P418

1. 准入标准： 流程开始前必须满足的条件
1. 任务、角色和参与者： 流程涉及的任务、角色和参与者
1. 验证步骤： 用于验证任务是否正确地完成
1. 退出标准： 用于指明流程已经成功完成的条件

<!-- slide -->
# CCP描述模板:book:P419图28-1

![httpatomoreillycomsourcemspimages1792359](./00_Image/httpatomoreillycomsourcemspimages1792359.png)

<!-- slide -->
# 变更请求的STD:book:P420图28-2

![httpatomoreillycomsourcemspimages1792360](./00_Image/httpatomoreillycomsourcemspimages1792360.png)

<!-- slide -->
# CCB章程

## 每个项目都应当创建一个简短的章程来描述CCB的目的、职责范围、成员、操作流程和决策流程

<!-- slide -->
# 状态沟通与重新协商承诺

1. 更新状态： CCB做出决策后，指定的负责人在变更数据库中更新请求状态并通知提交人和其他受变更影响的人
1. 更新承诺： 在接受重大需求变更前，要重新与管理层和客户再次协商承诺以适应变更，可以申请更多时间或将低优先级的需求搁置一旁，如果未能获得调整，将影响项目成功的威胁记录在风险列表中以便项目出现负面结果时让人们有心理准备

<!-- slide -->
# 变更控制工具Change Control Tools:book:P424

1. 允许定义变更请求的属性
1. 允许通过多个变更请求的状态实现一个变更请求生命周期
1. 强制按照CCP的状态转换模型执行，仅让授权的用户可以执行特定的状态变更
1. 记录每个状态变化的日期及操作人ID
1. 当提交人提交新的请求或一个请求的状态更新时，提供定制的自动生成的电子邮件通知
1. 同时提供标准的和可定制的报告与图表

<!-- slide -->
# 度量变更活动Measuring Change Activity:book:P425

1. 度量变更活动是一种评估需求稳定性的途径
1. 度量变更活动可以揭示未来减少变更的过程改进机会
1. 跟踪需求变更来源也有助于改进需求稳定性

<!-- slide -->
# 需求变更活动示例图:book:P425图28-3

![httpatomoreillycomsourcemspimages1792361](./00_Image/httpatomoreillycomsourcemspimages1792361.png)

<!-- slide -->
# 需求变更来源示例图:book:P426图28-4

![httpatomoreillycomsourcemspimages1792362](./00_Image/httpatomoreillycomsourcemspimages1792362.png)

<!-- slide -->
# 变更影响分析Change Impact Analysis:book:P426

1. 影响分析是一个负责任的需求管理的重要表现
1. 影响分析提供变更复杂性的准确理解，帮助团队做出明智的决策
1. 影响分析估算实现变更所需的投入
1. 影响分析的步骤：
    1. 理解变更可能导致的影响
    1. 识别变更导致的所有需要修改的需求、文件、模型和文档
    1. 识别为实现变更所需要的任务，并估算所需要的投入

<!-- slide -->
# 敏捷项目的变更管理

1. 敏捷项目通过维护动态的待完成工作列表Backlog来管理变更
1. 每个迭代实现Backlog中当时具有最高优先级的一组工作项
1. 客户或产品负责人对Backlog进行优先级排序并负主要责任

<!-- slide -->
# 敏捷项目的动态Backlog:book:P430图28-9

![httpatomoreillycomsourcemspimages1792367](./00_Image/httpatomoreillycomsourcemspimages1792367.png)

<!-- slide -->
# Thank You

## :ok:End of This Chapter:ok:
